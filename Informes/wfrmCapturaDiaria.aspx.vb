﻿
Partial Class Informes_wfrmCapturaDiaria
    Inherits System.Web.UI.Page

    Protected Sub CalFecha_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CalFecha.SelectionChanged
        Me.txtFecha.Text = Mid(Me.CalFecha.SelectedDate.ToString, 1, 10)
        Me.CalFecha.Visible = False
    End Sub

    Protected Sub btnFecha_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFecha.Click
        If Me.CalFecha.Visible = True Then
            Me.CalFecha.Visible = False
        Else
            Me.CalFecha.Visible = True
        End If
    End Sub

    Protected Sub btnGenerar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerar.Click
        Response.Redirect("CorteCaptura.pdf")
    End Sub
End Class
