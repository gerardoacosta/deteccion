﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mspPrincipal.master" AutoEventWireup="false" CodeFile="wfrmAvanceDeEstructura.aspx.vb" Inherits="Informes_wfrmListadoporEstructura" %><%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">

    .style4
    {
        height: 29px;
    }



    .style3
    {
        width: 196px;
    }

        .style8
    {
        width: 104px;
    }

        .style2
        {
            height: 21px;
        }
    .style6
    {
        width: 157px;
    }
    .style7
    {
        width: 248px;
    }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <table style="width:100%;">
        <tr>
            <td colspan="2" class="style4">
                <asp:Label ID="Label6" runat="server" Font-Bold="True" Font-Size="Large" 
                    Text="Avance de Estructura " ForeColor="#FF9933"></asp:Label>
                <asp:ScriptManager ID="ScriptManager2" runat="server">
                </asp:ScriptManager>
            </td>
        </tr>
        <tr>
            <td class="style3" valign="top">
                            <asp:TreeView ID="trvTerritorio" runat="server" Height="558px" Width="214px" 
                                ShowLines="True">
                                <Nodes>
                                    <asp:TreeNode Target="D" Text="Distrito 1" Value="1">
                                        <asp:TreeNode Target="SS" Text="Sec 1, 2, 3, 4, 5" Value="1, 2, 3, 4, 5">
                                            <asp:TreeNode Target="S" Text="Sec 1" Value="1">
                                                <asp:TreeNode Target="MM" Text="Mzas 1,2,3" Value="1,2,3">
                                                    <asp:TreeNode Target="M" Text="Mza 1" Value="1"></asp:TreeNode>
                                                </asp:TreeNode>
                                            </asp:TreeNode>
                                        </asp:TreeNode>
                                    </asp:TreeNode>
                                    <asp:TreeNode Target="D" Text="Distrito 2" Value="2"></asp:TreeNode>
                                </Nodes>
                                <SelectedNodeStyle BorderColor="#99CCFF" />
                            </asp:TreeView>
            </td>
            <td align="left" class="style4" valign="top">
                <table style="width:100%;">
                    <tr>
                        <td class="style8" rowspan="3">
                            <asp:Label ID="Label7" runat="server" Font-Bold="True" Font-Size="Medium" 
                Text="Por Sección:" ForeColor="#506272"></asp:Label>
                        </td>
                        <td>
                            <asp:DropDownList ID="DropDownList2" runat="server" Height="16px" 
                            style="margin-left: 0px" Width="98px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
                <rsweb:reportviewer ID="reportviewer2" runat="server" Width="724px">
                </rsweb:reportviewer>
            </td>
        </tr>
        <tr>
            <td class="style2" colspan="2">
                <table style="width:100%;">
                    <tr>
                        <td class="style6">
                        <asp:Button ID="btnGenerar" runat="server" Text="Generar" />
                        </td>
                        <td class="style7">
                        &nbsp;</td>
                        <td>
                        &nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>

