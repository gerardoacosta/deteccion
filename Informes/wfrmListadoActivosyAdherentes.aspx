﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mspPrincipal.master" AutoEventWireup="false" CodeFile="wfrmListadoActivosyAdherentes.aspx.vb" Inherits="Informes_wfrmListadoActivosyAdherentes" %>

<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">




    .style4
    {
        height: 29px;
    }



    .style3
    {
        width: 196px;
    }

        .style8
    {
        width: 104px;
    }

        .style2
        {
            height: 21px;
        }
    .style6
    {
        width: 157px;
    }
    .style7
    {
        width: 248px;
    }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <table style="width:100%;">
    <tr>
        <td colspan="2" class="style4">
            <asp:Label ID="Label2" runat="server" Font-Bold="True" Font-Size="Large" 
                    Text="Listados de Activos y Adherentes" ForeColor="#FF9933"></asp:Label>
            <asp:ScriptManager ID="ScriptManager1" runat="server">
            </asp:ScriptManager>
        </td>
    </tr>
    <tr>
        <td class="style3" valign="top">
            <table style="width:100%;">
                <tr>
                    <td>
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label6" runat="server" Text="Distrito:" Font-Bold="True" 
                            ForeColor="#506272"></asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="DropDownList2" runat="server" Height="21px" Width="110px">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label7" runat="server" Text="Municipio:" Font-Bold="True" 
                            ForeColor="#506272"></asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="DropDownList3" runat="server" Height="16px" Width="110px">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label8" runat="server" Text="Sección:" Font-Bold="True" 
                            ForeColor="#506272"></asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="DropDownList4" runat="server" Height="18px" Width="110px">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:Label ID="Label12" runat="server" Text="Mostrar" 
                            Font-Bold="True" ForeColor="#FF9933"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:RadioButton ID="RadioButton1" runat="server" Font-Bold="True" 
                            ForeColor="#506272" Text="Activos" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:RadioButton ID="RadioButton2" runat="server" Font-Bold="True" 
                            ForeColor="#506272" Text="Adherentes" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td>
                        &nbsp;</td>
                    <td>
                        <asp:Button ID="btnGenerar" runat="server" Text="Generar" />
                    </td>
                </tr>
            </table>
        </td>
        <td align="left" class="style4" valign="top">
            <table style="width:100%;">
                <tr>
                    <td class="style8" rowspan="3">
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
            </table>
            <rsweb:reportviewer ID="ReportViewer1" runat="server" Width="724px">
            </rsweb:reportviewer>
        </td>
    </tr>
    <tr>
        <td class="style2" colspan="2">
            <table style="width:100%;">
                <tr>
                    <td class="style6">
                        &nbsp;</td>
                    <td class="style7">
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</asp:Content>

