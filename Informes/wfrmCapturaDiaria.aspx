﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mspPrincipal.master" AutoEventWireup="false" CodeFile="wfrmCapturaDiaria.aspx.vb" Inherits="Informes_wfrmCapturaDiaria" %>

<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">





    .style4
    {
        height: 29px;
    }



    .style3
    {
        width: 196px;
    }

        .style8
    {
        width: 104px;
    }

        .style2
        {
            height: 21px;
        }
    .style6
    {
        width: 157px;
    }
    .style7
    {
        width: 248px;
    }
    .style9
    {
        width: 91px;
    }
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <table style="width:100%;">
    <tr>
        <td colspan="2" class="style4">
            <asp:Label ID="Label2" runat="server" Font-Bold="True" Font-Size="Large" 
                    Text="Captura Diaria" ForeColor="#FF9933"></asp:Label>
            <asp:ScriptManager ID="ScriptManager1" runat="server">
            </asp:ScriptManager>
        </td>
    </tr>
    <tr>
        <td class="style3" valign="top">
            <table style="width:100%;">
                <tr>
                    <td class="style9">
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="style9">
                        <asp:Label ID="Label6" runat="server" Text="Fecha:" Font-Bold="True" 
                            ForeColor="#506272"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtFecha" runat="server" ReadOnly="True" Width="80px"></asp:TextBox>
                        <asp:Button ID="btnFecha" runat="server" Text="..." />
                    </td>
                </tr>
                <tr>
                    <td class="style9">
                        &nbsp;</td>
                    <td>
                        <asp:Calendar ID="CalFecha" runat="server" Visible="False"></asp:Calendar>
                    </td>
                </tr>
                <tr>
                    <td class="style9">
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="style9">
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="style9">
                        &nbsp;</td>
                    <td>
                        <asp:Button ID="btnGenerar" runat="server" Text="Generar" />
                    </td>
                </tr>
            </table>
        </td>
        <td align="left" class="style4" valign="top">
            <table style="width:100%;">
                <tr>
                    <td class="style8" rowspan="3">
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
            </table>
            <rsweb:reportviewer ID="ReportViewer1" runat="server" Width="724px">
            </rsweb:reportviewer>
        </td>
    </tr>
    <tr>
        <td class="style2" colspan="2">
            <table style="width:100%;">
                <tr>
                    <td class="style6">
                        &nbsp;</td>
                    <td class="style7">
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</asp:Content>

