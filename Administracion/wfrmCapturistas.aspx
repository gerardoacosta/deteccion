﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mspPrincipal.master" AutoEventWireup="false" CodeFile="wfrmCapturistas.aspx.vb" Inherits="Administracion_wfrmCapturistas" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">

    .style22
    {
        height: 70px;
    }
    .style18
    {
        height: 4px;
        width: 363px;
    }
    .style23
    {
        height: 4px;
        width: 356px;
    }
    .style16
    {
        width: 161px;
        height: 22px;
    }
    .style19
    {
        height: 22px;
        width: 363px;
    }
    .style11
    {
        height: 22px;
    }
    .style17
    {
        width: 161px;
        height: 20px;
    }
    .style14
    {
            width: 161px;
            height: 29px;
        }
    .style15
    {
        height: 29px;
    }
        .style24
        {
            height: 23px;
        }
        .style25
        {
            width: 161px;
            height: 4px;
        }
        .style26
        {
            width: 186px;
            height: 12px;
        }
        .style27
        {
            height: 12px;
            width: 363px;
        }
        .style28
        {
            height: 12px;
        }
        .style29
        {
            width: 330px;
            height: 5px;
        }
        .style31
        {
            height: 5px;
            width: 470px;
        }
        .style32
        {
            height: 5px;
            width: 363px;
        }
        .style33
        {
            width: 330px;
            height: 8px;
        }
        .style35
        {
            height: 8px;
            width: 470px;
        }
        .style36
        {
            height: 8px;
            width: 363px;
        }
        .style37
        {
            width: 330px;
        }
        .style38
        {
            width: 363px;
        }
        .style39
        {
            width: 470px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <table style="width:100%; margin-right: 0px;">
        <tr>
            <td colspan="3" align="center" class="style22" 
            style="background-image: url('../Imagenes/Fondo_Subtitulo.png')">
                <asp:Label ID="Label2" runat="server" Font-Bold="True" Font-Size="Large" 
                    Text="Capturistas" ForeColor="#FF9933"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="style25">
            </td>
            <td class="style18">
            </td>
            <td class="style23">
            </td>
        </tr>
        <tr>
            <td class="style16">
                <asp:Label ID="Label1" runat="server" Text="Número a crear:" Font-Bold="True" 
                ForeColor="#506272"></asp:Label>
            </td>
            <td class="style19">
                <asp:TextBox ID="TextBox1" runat="server" MaxLength="2" 
                style="margin-left: 0px" Width="37px"></asp:TextBox>
            </td>
            <td class="style11">
            </td>
        </tr>
        <tr>
            <td class="style26">
                <asp:Label ID="Label3" runat="server" Text="Usuario:" Font-Bold="True" 
                ForeColor="#506272"></asp:Label>
            </td>
            <td class="style27">
                <asp:TextBox ID="TextBox2" runat="server" style="margin-left: 0px"></asp:TextBox>
            </td>
            <td class="style28">
            </td>
        </tr>
        <tr>
            <td class="style29">
                <asp:Label ID="Label4" runat="server" Text="Clave:" Font-Bold="True" 
                ForeColor="#506272"></asp:Label>
            </td>
            <td class="style32">
                <asp:TextBox ID="TextBox3" runat="server" style="margin-left: 0px"></asp:TextBox>
            </td>
            <td class="style31">
            </td>
        </tr>
        <tr>
            <td class="style33">
            </td>
            <td class="style36">
                <asp:Button ID="btnCargar" runat="server" Text="Aceptar" Width="77px" />
&nbsp;<asp:Button ID="btnCancelar" runat="server" Text="Cancelar" />
            </td>
            <td class="style35">
            </td>
        </tr>
        <tr>
            <td class="style37">
            </td>
            <td class="style38">
            </td>
            <td class="style39">
            </td>
        </tr>
        <tr>
            <td colspan="3">
                &nbsp;</td>
        </tr>
        <tr>
            <td colspan="3" class="style24">
            </td>
        </tr>
        <tr>
            <td class="style2" colspan="3">
&nbsp;&nbsp;&nbsp;
                </td>
        </tr>
    </table>
</asp:Content>

