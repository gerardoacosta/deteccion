﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mspPrincipal.master" AutoEventWireup="false" CodeFile="wfrmCargaSecciones.aspx.vb" Inherits="Administracion_wfrmCargaSecciones" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
    <style type="text/css">
        .style1
        {
        }
        .style2
        {
            height: 21px;
        }
    .style3
    {
        width: 427px;
    }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <table style="width:100%;">
        <tr>
            <td colspan="3">
                <asp:Label ID="Label2" runat="server" Font-Bold="True" Font-Size="Large" 
                    Text="Carga de Secciones Electorales Prioritarias" ForeColor="#FF9933"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="style3">
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style3">
                <asp:Label ID="Label1" runat="server" Text="Archivo:" Font-Bold="True" 
                    ForeColor="#506272"></asp:Label>
                <asp:FileUpload ID="FileUpload1" runat="server" Width="367px" />
            </td>
            <td>
                <asp:Button ID="btnVerificar" runat="server" Text="Verificar" />
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style3">
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
                    Height="40px">
                    <Columns>
                        <asp:BoundField HeaderText="Sección" />
                        <asp:BoundField HeaderText="Prioridad" />
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td class="style2" colspan="3">
            </td>
        </tr>
        <tr>
            <td class="style2" colspan="3">
                <asp:Button ID="btnCargar" runat="server" Text="Cargar" Width="77px" />
&nbsp;&nbsp;&nbsp;
                <asp:Button ID="btnCancelar" runat="server" Text="Cancelar" />
            </td>
        </tr>
    </table>
</asp:Content>

