﻿Imports AspMap
Imports AspMap.Web
Imports System.Drawing

Partial Class Territorios_wfrmMapa
    Inherits System.Web.UI.Page
    Dim layer As AspMap.Layer
    Dim strRutaMapas As String = "c:/Mexico2006/"
    Dim point As AspMap.Point
    Dim IdentifyRS As AspMap.Recordset
    Dim idEstado As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load

        idEstado = Session("Estado")

        Select Case Session("NivelMap")
            Case 1 'DISTRITO, MUNICIPIO
                CargaMapa()

            Case 2 'SECCIONES
                CargaSecciones()

            Case 3 'SECCIONES
                CargaSecciones()

            Case 4 'MANZANAS
                CargaManzanas()

            Case 5 'MANZANAS
                CargaManzanas()

        End Select



    End Sub

    Private Sub CargaMapa()
        map.MapUnit = MeasureUnit.Degree
        map.ScaleBar.Visible = True
        map.ScaleBar.BarUnit = UnitSystem.Imperial

        If Me.rdbDistritos.Checked Then
            layer = map.AddLayer(strRutaMapas & "/" & idEstado & "/Distrito.tab")
            Me.map.Layer("distrito").ShowLabels = True
            Me.map.Layer("distrito").LabelField = "distrito"
        Else
            layer = map.AddLayer(strRutaMapas & "/" & idEstado & "/Municipio.tab")
            Me.map.Layer("municipio").ShowLabels = True
            Me.map.Layer("municipio").LabelField = "Nombre"

        End If

        layer.Symbol.Size = 1
        layer.Symbol.LineColor = Color.FromArgb(103, 103, 52)
        layer.Symbol.FillColor = Color.FromArgb(199, 199, 141)

    End Sub

    Private Sub CargaSecciones()
        map.MapUnit = MeasureUnit.Degree
        map.ScaleBar.Visible = True
        map.ScaleBar.BarUnit = UnitSystem.Imperial

        layer = map.AddLayer(strRutaMapas & "/" & idEstado & "/SECCION.Tab")
        Me.map.Layer("seccion").ShowLabels = True
        Me.map.Layer("seccion").LabelField = "seccion"

        layer.Symbol.Size = 1
        layer.Symbol.LineColor = Color.FromArgb(103, 103, 52)
        layer.Symbol.FillColor = Color.FromArgb(199, 199, 141)

    End Sub

    Private Sub CargaManzanas()
        layer = map.AddLayer(strRutaMapas & "/" & idEstado & "/SECCION.Tab")
        layer = map.AddLayer(strRutaMapas & "/" & idEstado & "/MANZANA.Tab")
        Me.map.Layer("manzana").ShowLabels = True
        Me.map.Layer("manzana").LabelField = "manzana"
        layer.Symbol.Size = 2
        layer.Symbol.LineColor = Color.FromArgb(82, 129, 163)
        layer.Symbol.FillColor = Color.FromArgb(122, 160, 188)
    End Sub

    Protected Sub btnZoomAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnZoomAll.Click
        map.ZoomFull()
    End Sub

    Protected Sub btnZoomIn_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnZoomIn.Click
        map.MapTool = MapTool.ZoomIn
    End Sub


    Protected Sub btnZoomOut_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnZoomOut.Click
        map.MapTool = MapTool.ZoomOut
    End Sub

    Protected Sub btnMover_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnMover.Click
        map.MapTool = MapTool.Pan
    End Sub

    Protected Sub btnCursor_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCursor.Click
        map.MapTool = MapTool.Center
    End Sub

    Protected Sub btnLinea_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLinea.Click
        map.MapTool = MapTool.Line
    End Sub

    Protected Sub rdbDistrito_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdbDistritos.CheckedChanged
        Call CargaMapa()
        Me.rdbMunicipios.Checked = False
    End Sub

    Protected Sub rdbMunicipio_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdbMunicipios.CheckedChanged
        Call CargaMapa()
        Me.rdbDistritos.Checked = False
    End Sub

    Protected Sub chkIglesia_CheckedChanged(sender As Object, e As System.EventArgs) Handles chkIglesia.CheckedChanged

    End Sub

    Protected Sub chkCalles_CheckedChanged(sender As Object, e As System.EventArgs) Handles chkCalles.CheckedChanged

    End Sub

    Protected Sub map_LineTool(ByVal sender As Object, ByVal e As AspMap.Web.LineToolEventArgs) Handles map.LineTool
        map.MapShapes.Clear()

        Dim mapShape As MapShape = map.MapShapes.Add(e.Line)
        mapShape.Symbol.Size = 2
        mapShape.Symbol.LineColor = Color.Red

        Dim activeLayer As AspMap.Layer = map.FindLayer("1") 'layerList.SelectedValue)

        If activeLayer IsNot Nothing Then
            Dim records As AspMap.Recordset = activeLayer.SearchShape(e.Line, SearchMethod.Intersect)

            If (Not records.EOF) Then
                txtNombre.Text = records.Fields.Item(records.Fields.Count).Value
                
            End If
        End If
    End Sub

    Protected Sub trvTerritorio_SelectedNodeChanged(sender As Object, e As System.EventArgs) Handles trvTerritorio.SelectedNodeChanged
        Dim nodMenu As TreeNode
        nodMenu = Me.trvTerritorio.SelectedNode

        Select Case nodMenu.Target
            Case "D"
                Session("NivelMap") = "1"
                Call CargaMapa()
                Dim feature As AspMap.Feature
                feature = map("distrito").Renderer.Add
                feature.Expression = "distrito=" & nodMenu.Value
                feature.Symbol.FillColor = Color.FromArgb(179, 217, 255)
                feature.Symbol.LineColor = Color.FromArgb(100, 100, 255)
                feature.Symbol.LineStyle = LineStyle.Solid
                feature.Symbol.Size = 4

            Case "SS"
                Session("NivelMap") = "2"
                Call CargaSecciones()
                Dim feature As AspMap.Feature
                For i = 1 To 5
                    feature = map("seccion").Renderer.Add
                    feature.Expression = "seccion = " & i
                    feature.Symbol.FillColor = Color.FromArgb(179, 217, 255)
                    feature.Symbol.LineColor = Color.FromArgb(100, 100, 255)
                    feature.Symbol.LineStyle = LineStyle.Solid
                    feature.Symbol.Size = 4
                Next

            Case "S"
                Session("NivelMap") = "3"
                Call CargaSecciones()
                Dim feature As AspMap.Feature
                feature = map("seccion").Renderer.Add
                feature.Expression = "seccion=" & nodMenu.Value & " AND seccion=5"
                feature.Symbol.FillColor = Color.FromArgb(179, 217, 255)
                feature.Symbol.LineColor = Color.FromArgb(100, 100, 255)
                feature.Symbol.LineStyle = LineStyle.Solid
                feature.Symbol.Size = 4

            Case "MM"
                Session("NivelMap") = "4"
                Call CargaManzanas()
                Dim feature As AspMap.Feature
                feature = map("seccion").Renderer.Add
                feature.Expression = "seccion=" & nodMenu.Value
                feature.Symbol.FillColor = Color.FromArgb(179, 217, 255)
                feature.Symbol.LineColor = Color.FromArgb(100, 100, 255)
                feature.Symbol.LineStyle = LineStyle.Solid
                feature.Symbol.Size = 4

            Case "M"
                Session("NivelMap") = "5"
                Call CargaManzanas()
                Dim feature As AspMap.Feature
                feature = map("seccion").Renderer.Add
                feature.Expression = "seccion=" & nodMenu.Value
                feature.Symbol.FillColor = Color.FromArgb(179, 217, 255)
                feature.Symbol.LineColor = Color.FromArgb(100, 100, 255)
                feature.Symbol.LineStyle = LineStyle.Solid
                feature.Symbol.Size = 4
        End Select

    End Sub
End Class
