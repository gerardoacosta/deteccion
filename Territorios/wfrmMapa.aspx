﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mspPrincipal.master" AutoEventWireup="false" CodeFile="wfrmMapa.aspx.vb" Inherits="Territorios_wfrmMapa" %>

<%@ Register assembly="AspMapNET" namespace="AspMap.Web" tagprefix="aspmap" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .style13
        {
            width: 81px;
            height: 4px;
        }
        .style14
        {
            width: 48px;
            height: 34px;
        }
        .style15
        {
            height: 34px;
        }
        .style11
        {
            width: 81px;
            height: 424px;
        }
    .style9
    {
        height: 424px;
    }
    .style10
    {
        width: 185px;
    }
        .style12
        {
            width: 230px;
        }
        .style16
        {
            width: 48px;
        }
        .style18
        {
            width: 81px;
        }
        .style19
        {
            width: 632px;
        }
        .style20
        {
            width: 330px;
            height: 4px;
        }
        .style21
        {
            height: 4px;
            width: 470px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" runat="server" 
    contentplaceholderid="ContentPlaceHolder2">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:ScriptManager ID="ScriptManager1" runat="server">
            </asp:ScriptManager>
            <table style="width: 100%;">
                <tr>
                    <td class="style13">
                    </td>
                    <td class="style20">
                        <asp:ImageButton ID="btnZoomAll" runat="server" 
                            ImageUrl="~/Imagenes/zoomfull.gif" />
                        <asp:ImageButton ID="btnZoomIn" runat="server" 
                            ImageUrl="~/Imagenes/zoomin.gif" />
                        <asp:ImageButton ID="btnZoomOut" runat="server" 
                            ImageUrl="~/Imagenes/zoomout.gif" />
                        <asp:ImageButton ID="btnMover" runat="server" ImageUrl="~/Imagenes/pan.gif" />
                        <asp:ImageButton ID="btnCursor" runat="server" 
                            ImageUrl="~/Imagenes/arrow.gif" />
                        <asp:ImageButton ID="btnLinea" runat="server" ImageUrl="~/Imagenes/line.gif" />
                        <asp:ImageButton ID="btnLinea0" runat="server" 
                            ImageUrl="~/Imagenes/polyline.gif" />
                        <asp:ImageButton ID="btnLinea1" runat="server" 
                            ImageUrl="~/Imagenes/circle.gif" />
                        <asp:ImageButton ID="btnLinea2" runat="server" 
                            ImageUrl="~/Imagenes/polygon.gif" />
                        <asp:Label ID="Label8" runat="server" Text="Nombre:"></asp:Label>
                        <asp:TextBox ID="txtNombre" runat="server" Width="230px"></asp:TextBox>
                        <asp:Button ID="Button1" runat="server" Text="Guardar" />
                    </td>
                    <td class="style21" valign="top">
                    </td>
                </tr>
                <tr>
                    <td class="style11">
                        <asp:Panel ID="Panel1" runat="server" Height="565px" Width="223px">
                            <asp:TreeView ID="trvTerritorio" runat="server" Height="558px" Width="163px" 
                                ShowLines="True">
                                <Nodes>
                                    <asp:TreeNode Target="D" Text="Distrito 1" Value="1">
                                        <asp:TreeNode Target="SS" Text="Sec 1, 2, 3, 4, 5" Value="1, 2, 3, 4, 5">
                                            <asp:TreeNode Target="S" Text="Sec 1" Value="1">
                                                <asp:TreeNode Target="MM" Text="Mzas 1,2,3" Value="1,2,3">
                                                    <asp:TreeNode Target="M" Text="Mza 1" Value="1"></asp:TreeNode>
                                                </asp:TreeNode>
                                            </asp:TreeNode>
                                        </asp:TreeNode>
                                    </asp:TreeNode>
                                    <asp:TreeNode Target="D" Text="Distrito 2" Value="2"></asp:TreeNode>
                                </Nodes>
                                <SelectedNodeStyle BorderColor="#99CCFF" />
                            </asp:TreeView>
                        </asp:Panel>
                    </td>
                    <td>
                        <aspmap:Map ID="map" runat="server" BackColor="#F0F8FF" BorderColor="#8080FF" 
                            BorderStyle="Solid" BorderWidth="1px" FontQuality="ClearType" Height="551px" 
                            ImageFormat="Gif" SmoothingMode="AntiAlias" style="margin-left: 47px" 
                            Width="728px" />
                    </td>
                    <td class="style9" valign="top">
                        <table style="width:164%;">
                            <tr>
                                <td class="style19">
                                    Visualización</td>
                            </tr>
                            <tr>
                                <td class="style19">
                                    <asp:RadioButton ID="rdbDistritos" runat="server" AutoPostBack="True" 
                                        Text="Distritos" Checked="True" />
                                </td>
                            </tr>
                            <tr>
                                <td class="style19">
                                    <asp:RadioButton ID="rdbMunicipios" runat="server" Text="Municipios" 
                                        AutoPostBack="True" />
                                </td>
                            </tr>
                            <tr>
                                <td class="style19">
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <td class="style19">
                                    Referencias</td>
                            </tr>
                            <tr>
                                <td class="style19">
                                    <asp:CheckBox ID="chkEscuelas" runat="server" Text="Escuelas" 
                                        AutoPostBack="True" />
                                </td>
                            </tr>
                            <tr>
                                <td class="style19">
                                    <asp:CheckBox ID="chkIglesia" runat="server" Text="Iglesias" 
                                        AutoPostBack="True" />
                                </td>
                            </tr>
                            <tr>
                                <td class="style19">
                                    <asp:CheckBox ID="chkParques" runat="server" Text="Parques" 
                                        AutoPostBack="True" />
                                </td>
                            </tr>
                            <tr>
                                <td class="style19">
                                    <asp:CheckBox ID="chkCalles" runat="server" Text="Calles" AutoPostBack="True" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="style18">
                        &nbsp;</td>
                    <td class="style16">
                        &nbsp;</td>
                    <td valign="top">
                        &nbsp;</td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>


