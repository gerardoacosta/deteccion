﻿Imports Microsoft.VisualBasic
Imports System
Imports System.Collections
Imports System.Configuration.Assemblies
Imports System.Diagnostics
Imports System.Globalization

Imports System.IO

Imports System.Reflection
Imports System.Resources
Imports System.Runtime.CompilerServices
Imports System.Runtime.InteropServices
Imports System.Runtime.Serialization
Imports System.Runtime.Serialization.Formatters.Binary
Imports System.Security
Imports System.Security.Cryptography.X509Certificates
Imports System.Security.Permissions
Imports System.Security.Policy
Imports System.Text
Imports System.Threading

Imports System.ComponentModel
Imports System.Data
Imports System.Drawing
Imports System.Web
Imports System.Web.SessionState
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls

Imports AspMap
Imports AspMap.Web
Partial Class Documentos_wfrmMapasDefault
    Inherits System.Web.UI.Page
    Dim layer As AspMap.Layer
    Dim strRutaMapas As String = "c:/Mexico2006/"
    Dim point As AspMap.Point
    Dim IdentifyRS As AspMap.Recordset

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ''If Not Me.Page.IsPostBack Then
        Call CargaMexico()

        ''End If
    End Sub

    Private Sub CargaMexico()
        map.MapUnit = MeasureUnit.Degree
        map.ScaleBar.Visible = True
        map.ScaleBar.BarUnit = UnitSystem.Imperial

        ''Mexico
        layer = map.AddLayer(strRutaMapas & "/16/Municipio.tab")
        Me.map.Layer("municipio").ShowLabels = True
        Me.map.Layer("municipio").LabelField = "nombre"
        layer.Symbol.Size = 2
        layer.Symbol.LineColor = Color.FromArgb(82, 129, 163)
        layer.Symbol.FillColor = Color.FromArgb(122, 160, 188)

        ''layer = map.AddLayer(strRutaMapas & "/16/Colonia.tab")
        ''Me.map.Layer("colonia").ShowLabels = True
        ''Me.map.Layer("colonia").LabelField = "colonia"
        ''layer.Symbol.Size = 2
        ''layer.Symbol.LineColor = Color.FromArgb(99, 98, 71)
        ''layer.Symbol.FillColor = Color.FromArgb(136, 135, 98)
        ''layer.Symbol.PointFont.Bold = True

        layer = map.AddLayer(strRutaMapas & "/16/Manzana.tab")
        Me.map.Layer("manzana").ShowLabels = True
        Me.map.Layer("manzana").LabelField = "manzana"
        layer.Symbol.Size = 2
        layer.Symbol.LineColor = Color.FromArgb(82, 129, 163)
        layer.Symbol.FillColor = Color.FromArgb(122, 160, 188)

        layer = map.AddLayer(strRutaMapas & "/16/Vialidad.tab")
        Me.map.Layer("vialidad").ShowLabels = True
        Me.map.Layer("vialidad").LabelField = "nombre"
        layer.Symbol.Size = 5
        layer.Symbol.LineColor = Color.FromArgb(103, 103, 52)
        layer.Symbol.FillColor = Color.FromArgb(199, 199, 141)

        layer = map.AddLayer(strRutaMapas & "/16/pasto.tab")
        Me.map.Layer("pasto").ShowLabels = True
        Me.map.Layer("pasto").LabelField = "pasto"
        layer.Symbol.Size = 2
        layer.Symbol.LineColor = Color.ForestGreen
        layer.Symbol.FillColor = Color.ForestGreen

    End Sub

    Protected Sub btnZoomAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnZoomAll.Click
        map.ZoomFull()
    End Sub

    Protected Sub btnZoomIn_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnZoomIn.Click
        map.MapTool = MapTool.ZoomIn
    End Sub


    Protected Sub btnZoomOut_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnZoomOut.Click
        map.MapTool = MapTool.ZoomOut
    End Sub

    Protected Sub btnMover_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnMover.Click
        map.MapTool = MapTool.Pan
    End Sub

    Protected Sub btnCursor_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCursor.Click
        map.MapTool = MapTool.Center
    End Sub

    Protected Sub btnLinea_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLinea.Click
        map.MapTool = MapTool.Line
    End Sub

End Class
