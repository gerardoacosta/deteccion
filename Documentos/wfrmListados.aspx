﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mspPrincipal.master" AutoEventWireup="false" CodeFile="wfrmListados.aspx.vb" Inherits="Documentos_wfrmListado" %><%@ Register assembly="AspMapNET" namespace="AspMap.Web" tagprefix="aspmap" %>
<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
    .style4
    {
        height: 29px;
    }



    .style3
    {
        width: 196px;
    }

        .style2
        {
            height: 21px;
        }
    .style6
    {
        width: 157px;
    }
    .style7
    {
        width: 248px;
    }
    .style8
    {
        width: 104px;
    }
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <table style="width:100%;">
    <tr>
        <td colspan="2" class="style4">
            <asp:Label ID="Label2" runat="server" Font-Bold="True" Font-Size="Large" 
                    Text="Listados de Localización" ForeColor="#FF9933"></asp:Label>
            <asp:ScriptManager ID="ScriptManager1" runat="server">
            </asp:ScriptManager>
        </td>
    </tr>
    <tr>
        <td class="style3" valign="top">
            <asp:TreeView ID="TreeView1" runat="server" Height="287px" ShowLines="True" 
                Width="190px">
                <Nodes>
                    <asp:TreeNode Text="Estructura" Value="Estructura">
                        <asp:TreeNode Text="Región" Value="Región">
                            <asp:TreeNode Text="Zona" Value="Zona">
                                <asp:TreeNode Text="Sector" Value="Sector"></asp:TreeNode>
                            </asp:TreeNode>
                        </asp:TreeNode>
                    </asp:TreeNode>
                </Nodes>
            </asp:TreeView>
        </td>
        <td align="left" class="style4" valign="top">
            <table style="width:100%;">
                <tr>
                    <td class="style8" rowspan="3">
                        <asp:Label ID="Label5" runat="server" Font-Bold="True" Font-Size="Medium" 
                Text="Por Sección:" ForeColor="#506272"></asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="DropDownList1" runat="server" Height="16px" 
                            style="margin-left: 0px" Width="98px">
                        </asp:DropDownList>
                    </td>
                </tr>
            </table>
            <rsweb:reportviewer ID="ReportViewer1" runat="server" Width="724px">
            </rsweb:reportviewer>
        </td>
    </tr>
    <tr>
        <td class="style2" colspan="2">
            <table style="width:100%;">
                <tr>
                    <td class="style6">
                        &nbsp;</td>
                    <td class="style7">
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</asp:Content>

