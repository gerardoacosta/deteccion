﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mspPrincipal.master" AutoEventWireup="false" CodeFile="wfrmMapas.aspx.vb" Inherits="Documentos_wfrmMapasDefault" %>

<%@ Register assembly="AspMapNET" namespace="AspMap.Web" tagprefix="aspmap" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">



    .style3
    {
        width: 196px;
    }

        .style2
        {
            height: 21px;
        }
    .style4
    {
        height: 29px;
    }
    .style5
    {
        height: 12px;
    }
    .style6
    {
        width: 157px;
    }
    .style7
    {
        width: 248px;
    }
        .style8
        {
            width: 71px;
            height: 14px;
        }
        .style9
        {
            height: 14px;
            width: 110px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <table style="width:100%;">
    <tr>
        <td colspan="2" class="style4">
            <asp:Label ID="Label2" runat="server" Font-Bold="True" Font-Size="Large" 
                    Text="Mapas de Localización" ForeColor="#FF9933"></asp:Label>
        </td>
    </tr>
    <tr>
        <td class="style8">
            </td>
        <td class="style9">
            <asp:Label ID="Label5" runat="server" Font-Bold="True" Font-Size="Medium" 
                Text="Por Sección:" ForeColor="#506272"></asp:Label>
            <asp:DropDownList ID="DropDownList1" runat="server" Height="16px" Width="98px">
            </asp:DropDownList>
                <asp:ImageButton ID="btnZoomAll" runat="server" 
                    ImageUrl="~/Imagenes/zoomfull.gif" />
                <asp:ImageButton ID="btnZoomIn" runat="server" 
                    ImageUrl="~/Imagenes/zoomin.gif" />
                <asp:ImageButton ID="btnZoomOut" runat="server" 
                    ImageUrl="~/Imagenes/zoomout.gif" />
                &nbsp;&nbsp;
                <asp:ImageButton ID="btnMover" runat="server" ImageUrl="~/Imagenes/pan.gif" />
                <asp:ImageButton ID="btnCursor" runat="server" 
                    ImageUrl="~/Imagenes/arrow.gif" />
                &nbsp;&nbsp;
                <asp:ImageButton ID="btnLinea" runat="server" ImageUrl="~/Imagenes/line.gif" />
                <asp:ImageButton ID="btnCircle" runat="server" 
                    ImageUrl="~/Imagenes/circle.gif" />
                <asp:ImageButton ID="btnRectangle" runat="server" 
                    ImageUrl="~/Imagenes/rectangle.gif" />
                <asp:ImageButton ID="btnPoligono" runat="server" 
                    ImageUrl="~/Imagenes/polygon.gif" />
                <asp:ImageButton ID="btnPunto" runat="server" ImageUrl="~/Imagenes/point.gif" />
        </td>
    </tr>
    <tr>
        <td class="style3" valign="top">
            <asp:TreeView ID="TreeView1" runat="server" Height="287px" ShowLines="True" 
                Width="190px">
                <Nodes>
                    <asp:TreeNode Text="Estructura" Value="Estructura">
                        <asp:TreeNode Text="Región" Value="Región">
                            <asp:TreeNode Text="Zona" Value="Zona">
                                <asp:TreeNode Text="Sector" Value="Sector"></asp:TreeNode>
                            </asp:TreeNode>
                        </asp:TreeNode>
                    </asp:TreeNode>
                </Nodes>
            </asp:TreeView>
        </td>
        <td align="left" class="style4" valign="top">
            <table style="width: 100%;">
                <tr>
                    <td rowspan="3">
            <aspmap:Map ID="map" runat="server" BackColor="#F0F8FF" BorderColor="#8080FF" 
                    BorderStyle="Solid" BorderWidth="1px" FontQuality="ClearType" Height="414px" 
                    ImageFormat="Gif" SmoothingMode="AntiAlias" Width="484px" />
                    </td>
                    <td class="style5">
                        <table style="width: 22%;">
                            <tr>
                                <td>
                                    &nbsp;</td>
                                <td>
                                    &nbsp;</td>
                                <td>
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;</td>
                                <td>
                                    &nbsp;</td>
                                <td>
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;</td>
                                <td>
                                    &nbsp;</td>
                                <td>
                                    &nbsp;</td>
                            </tr>
                        </table>
                    </td>
                    <td class="style5">
                    </td>
                </tr>
                <tr>
                    <td>
                        <table style="width: 100%;">
                            <tr>
                                <td>
                                    &nbsp;</td>
                                <td>
                                    &nbsp;</td>
                                <td>
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;</td>
                                <td>
                                    &nbsp;</td>
                                <td>
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;</td>
                                <td>
                                    &nbsp;</td>
                                <td>
                                    &nbsp;</td>
                            </tr>
                        </table>
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <table style="width: 100%;">
                            <tr>
                                <td>
                                    &nbsp;</td>
                                <td>
                                    &nbsp;</td>
                                <td>
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;</td>
                                <td>
                                    &nbsp;</td>
                                <td>
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;</td>
                                <td>
                                    &nbsp;</td>
                                <td>
                                    &nbsp;</td>
                            </tr>
                        </table>
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="style2" colspan="2">
            <table style="width: 100%;">
                <tr>
                    <td class="style6">
                        <asp:Label ID="Label6" runat="server" Font-Bold="True" Text="Encargado Sector:" 
                            ForeColor="#506272"></asp:Label>
                    </td>
                    <td class="style7">
                        <asp:Label ID="Label8" runat="server" Text="Nombre Encargado" 
                            ForeColor="#506272"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="Label9" runat="server" Text="Dirección Encargado" 
                            ForeColor="#506272"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style6">
                        &nbsp;</td>
                    <td class="style7">
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="style6">
                        <asp:Label ID="Label7" runat="server" Font-Bold="True" 
                            Text="Encargado Sección:" ForeColor="#506272"></asp:Label>
                    </td>
                    <td class="style7">
                        <asp:Label ID="Label10" runat="server" Text="Nombre Encargado" 
                            ForeColor="#506272"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="Label11" runat="server" Text="Dirección Encargado" 
                            ForeColor="#506272"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style6">
                        &nbsp;</td>
                    <td class="style7">
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
            </table>
        </td>
    </tr>
    </table>
</asp:Content>

