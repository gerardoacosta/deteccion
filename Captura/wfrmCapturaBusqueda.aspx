﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mspPrincipal.master" AutoEventWireup="false" CodeFile="wfrmCapturaBusqueda.aspx.vb" Inherits="Captura_wfrmCapturaBusqueda" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">

    .style12
    {
        width: 23px;
    }


        .style2
        {
            height: 21px;
        }
    .style9
    {            height: 42px;
        }
    .style7
    {
            width: 166px;
        }
    .style10
    {
        width: 81px;
    }
        .style13
        {
        }
        .style14
        {
        }
        .style15
        {
            width: 168px;
        }
        .style16
        {
            width: 214px;
        }
        .style17
        {
            width: 276px;
        }
        .style18
        {
            width: 102px;
        }
    .style19
    {
        width: 102px;
        height: 9px;
    }
    .style20
    {
        width: 168px;
        height: 9px;
    }
    .style21
    {
        width: 75px;
        height: 9px;
    }
    .style22
    {
        width: 166px;
        height: 9px;
    }
    .style23
    {
        width: 548px;
        height: 9px;
    }
    .style24
    {
        height: 9px;
    }
    .style25
    {
        width: 102px;
        height: 16px;
    }
    .style26
    {
        width: 168px;
        height: 16px;
    }
    .style27
    {
        width: 75px;
        height: 16px;
    }
    .style28
    {
        width: 166px;
        height: 16px;
    }
    .style29
    {
        width: 548px;
        height: 16px;
    }
    .style30
    {
        height: 16px;
    }
    .style31
    {
        height: 71px;
    }
        .style34
        {
            width: 356px;
            height: 8px;
        }
        .style35
        {
            width: 102px;
            height: 8px;
        }
        .style36
        {
            width: 168px;
            height: 8px;
        }
        .style37
        {
            width: 166px;
            height: 8px;
        }
        .style38
        {
            width: 548px;
            height: 8px;
        }
        .style39
        {
            height: 8px;
        }
        .style42
        {
            width: 356px;
            height: 12px;
        }
        .style43
        {
            width: 102px;
            height: 12px;
        }
        .style44
        {
            width: 168px;
            height: 12px;
        }
        .style45
        {
            width: 166px;
            height: 12px;
        }
        .style46
        {
            width: 548px;
            height: 12px;
        }
        .style47
        {
            height: 12px;
        }
        .style50
        {
            width: 356px;
        }
        .style52
        {
            width: 548px;
        }
        .style54
        {
            width: 371px;
        }
        .style55
        {
            height: 23px;
        }
        .style57
        {
            width: 470px;
        }
        .style58
        {
            height: 12px;
            width: 121px;
        }
        .style60
        {
            width: 330px;
        }
        .style61
        {
            width: 186px;
        }
        .style62
        {
            width: 266px;
        }
        .style64
        {
            width: 186px;
            height: 1px;
        }
        .style65
        {
            width: 266px;
            height: 1px;
        }
        .style66
        {
            height: 1px;
        }
        .style67
        {
            width: 186px;
            height: 25px;
        }
        .style68
        {
            width: 266px;
            height: 25px;
        }
        .style69
        {
            height: 25px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <table style="width:100%;">
    <tr>
        <td align="center" class="style31" 
            style="background-image: url('../Imagenes/Fondo_Subtitulo.png')">
            <asp:Label ID="Label2" runat="server" Font-Bold="True" Font-Size="Large" 
                    Text="Captura por Busqueda" ForeColor="#FF9933"></asp:Label>
        </td>
    </tr>
    <tr>
        <td bgcolor="#D5DADE">
            <asp:Label ID="Label28" runat="server" Font-Bold="True" ForeColor="#385692" 
                Text="Busqueda de Simpatizante"></asp:Label>
        </td>
    </tr>
    <tr>
        <td class="style2">
            <table style="width:100%; margin-bottom: 0px;">
                <tr>
                    <td class="style35">
                        <asp:Label ID="Label3" runat="server" Text="A.Paterno:" Font-Bold="True" 
                            ForeColor="#506272"></asp:Label>
                    </td>
                    <td class="style36">
                        <asp:TextBox ID="TextBox2" runat="server" style="margin-left: 0px" 
                            Width="145px"></asp:TextBox>
                    </td>
                    <td class="style34">
                        <asp:Label ID="Label4" runat="server" Text="A.Materno:" Font-Bold="True" 
                            ForeColor="#506272"></asp:Label>
                    </td>
                    <td class="style37">
                        <asp:TextBox ID="TextBox3" runat="server" style="margin-left: 0px" 
                            Width="145px"></asp:TextBox>
                    </td>
                    <td class="style38">
                        <asp:Label ID="Label5" runat="server" Text="Nombre:" Font-Bold="True" 
                            ForeColor="#506272"></asp:Label>
                    </td>
                    <td class="style39">
                        <asp:TextBox ID="TextBox4" runat="server" style="margin-left: 0px" 
                            Width="145px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="style43">
                        <asp:Label ID="Label6" runat="server" Text="Clave IFE:" Font-Bold="True" 
                            ForeColor="#506272"></asp:Label>
                    </td>
                    <td class="style44">
                        <asp:TextBox ID="TextBox5" runat="server" style="margin-left: 0px" 
                            Width="145px"></asp:TextBox>
                    </td>
                    <td class="style42">
                        <asp:Label ID="Label9" runat="server" Text="Municipio:" Font-Bold="True" 
                            ForeColor="#506272"></asp:Label>
                    </td>
                    <td class="style45">
                        <asp:DropDownList ID="DropDownList1" runat="server" Width="145px">
                        </asp:DropDownList>
                    </td>
                    <td class="style46">
                        <asp:Button ID="Button1" runat="server" Text="Buscar" />
                    </td>
                    <td class="style47">
                        </td>
                </tr>
                <tr>
                    <td class="style18">
                        </td>
                    <td class="style57">
                        </td>
                    <td class="style50">
                        </td>
                    <td class="style7">
                        </td>
                    <td class="style52">
                        </td>
                    <td>
                        </td>
                </tr>
                <tr>
                    <td class="style58" colspan="6">
                        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
                            Width="691px">
                            <Columns>
                                <asp:BoundField HeaderText="A.Paterno" />
                                <asp:BoundField HeaderText="A.Materno" />
                                <asp:BoundField HeaderText="Nombre" />
                                <asp:BoundField HeaderText="Dirección" />
                                <asp:BoundField HeaderText="Municipio" />
                                <asp:BoundField HeaderText="Intención" />
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
                <tr>
                    <td class="style13" colspan="6" bgcolor="#D5DADE">
                        <asp:Label ID="Label27" runat="server" Font-Bold="True" ForeColor="#385692" 
                            Text="Actualización de Información"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style19">
                        <asp:Label ID="Label21" runat="server" Text="A.Paterno:" Font-Bold="True" 
                            ForeColor="#506272"></asp:Label>
                    </td>
                    <td class="style20">
                        <asp:TextBox ID="TextBox8" runat="server" style="margin-left: 0px" 
                            Width="142px"></asp:TextBox>
                    </td>
                    <td class="style21">
                        <asp:Label ID="Label10" runat="server" Text="A.Materno:" Font-Bold="True" 
                            ForeColor="#506272"></asp:Label>
                    </td>
                    <td class="style22">
                        <asp:TextBox ID="TextBox14" runat="server" style="margin-left: 0px" 
                            Width="142px"></asp:TextBox>
                    </td>
                    <td class="style23">
                        <asp:Label ID="Label11" runat="server" Text="Nombre:" Font-Bold="True" 
                            ForeColor="#506272"></asp:Label>
                    </td>
                    <td class="style24">
                        <asp:TextBox ID="TextBox10" runat="server" style="margin-left: 0px" 
                            Width="142px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="style25">
                        <asp:Label ID="Label12" runat="server" Text="Calle:" Font-Bold="True" 
                            ForeColor="#506272"></asp:Label>
                    </td>
                    <td class="style26">
                        <asp:TextBox ID="TextBox11" runat="server" style="margin-left: 0px" 
                            Width="142px"></asp:TextBox>
                    </td>
                    <td class="style27">
                        <asp:Label ID="Label13" runat="server" Text="No. Ext.:" Font-Bold="True" 
                            ForeColor="#506272"></asp:Label>
                    </td>
                    <td class="style28">
                        <asp:TextBox ID="TextBox12" runat="server" style="margin-left: 0px" 
                            Width="142px"></asp:TextBox>
                    </td>
                    <td class="style29">
                        <asp:Label ID="Label14" runat="server" Text="No.Int.:" Font-Bold="True" 
                            ForeColor="#506272"></asp:Label>
                    </td>
                    <td class="style30">
                        <asp:TextBox ID="TextBox13" runat="server" style="margin-left: 0px" 
                            Width="142px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="style18">
                        <asp:Label ID="Label15" runat="server" Text="Colonia:" Font-Bold="True" 
                            ForeColor="#506272"></asp:Label>
                    </td>
                    <td class="style57">
                        <asp:TextBox ID="TextBox15" runat="server" style="margin-left: 0px" 
                            Width="142px"></asp:TextBox>
                    </td>
                    <td class="style50">
                        <asp:Label ID="Label16" runat="server" Text="C.P." Font-Bold="True" 
                            ForeColor="#506272"></asp:Label>
                    </td>
                    <td class="style7">
                        <asp:TextBox ID="TextBox16" runat="server" style="margin-left: 0px" 
                            Width="142px"></asp:TextBox>
                    </td>
                    <td class="style52">
                        <asp:Label ID="Label17" runat="server" Text="Municipio:" Font-Bold="True" 
                            ForeColor="#506272"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="TextBox17" runat="server" style="margin-left: 0px" 
                            Width="142px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="style18">
                        <asp:Label ID="Label22" runat="server" Text="Tel.Casa:" Font-Bold="True" 
                            ForeColor="#506272"></asp:Label>
                    </td>
                    <td class="style57">
                        <asp:TextBox ID="TextBox21" runat="server" style="margin-left: 0px" 
                            Width="142px"></asp:TextBox>
                    </td>
                    <td class="style50">
                        <asp:Label ID="Label23" runat="server" Text="Tel.Celular:" Font-Bold="True" 
                            ForeColor="#506272"></asp:Label>
                    </td>
                    <td class="style7">
                        <asp:TextBox ID="TextBox22" runat="server" style="margin-left: 0px" 
                            Width="142px"></asp:TextBox>
                    </td>
                    <td class="style52">
                        <asp:Label ID="Label24" runat="server" Text="Tel.Oficina:" Font-Bold="True" 
                            ForeColor="#506272"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="TextBox23" runat="server" style="margin-left: 0px" 
                            Width="142px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="style18">
                        <asp:Label ID="Label18" runat="server" Text="Email:" Font-Bold="True" 
                            ForeColor="#506272"></asp:Label>
                    </td>
                    <td class="style54" colspan="2">
                        <asp:TextBox ID="TextBox18" runat="server" style="margin-left: 0px" 
                            Width="242px"></asp:TextBox>
                    </td>
                    <td class="style7">
                        &nbsp;</td>
                    <td class="style52">
                        </td>
                    <td>
                        </td>
                </tr>
                <tr>
                    <td class="style18">
                        <asp:Label ID="Label20" runat="server" Text="Comentarios:" Font-Bold="True" 
                            ForeColor="#506272"></asp:Label>
                    </td>
                    <td class="style60" colspan="5">
                        <asp:TextBox ID="TextBox19" runat="server" style="margin-left: 0px" 
                            Width="646px"></asp:TextBox>
                    </td>
                </tr>
                </table>
            <table style="width:100%; height: 112px; margin-top: 0px; margin-bottom: 0px;">
                <tr>
                    <td class="style61" bgcolor="#006699">
                        <asp:Label ID="Label25" runat="server" Font-Bold="True" ForeColor="White" 
                            Text="Preferencia Electoral"></asp:Label>
                    </td>
                    <td class="style62" bgcolor="#006699">
                        <asp:Label ID="Label26" runat="server" Font-Bold="True" ForeColor="White" 
                            Text="Quiere Participar en la Campaña"></asp:Label>
                    </td>
                    <td bgcolor="#006699">
                        </td>
                </tr>
                <tr>
                    <td class="style64">
                        <asp:RadioButton ID="RadioButton1" runat="server" Font-Bold="True" 
                            ForeColor="#506272" Text="PAN" />
                    </td>
                    <td class="style65">
                        <asp:CheckBox ID="CheckBox1" runat="server" Font-Bold="True" 
                            ForeColor="#506272" Text="Conformando una RED de Apoyo" />
                    </td>
                    <td class="style66">
                        <asp:CheckBox ID="CheckBox4" runat="server" Font-Bold="True" 
                            ForeColor="#506272" Text="Como Representante de Casilla" />
                    </td>
                </tr>
                <tr>
                    <td class="style61">
                        <asp:RadioButton ID="RadioButton2" runat="server" Font-Bold="True" 
                            ForeColor="#506272" Text="PRI" />
                    </td>
                    <td class="style62">
                        <asp:CheckBox ID="CheckBox2" runat="server" Font-Bold="True" 
                            ForeColor="#506272" Text="Con Reparto de Propaganda" />
                    </td>
                    <td>
                        <asp:CheckBox ID="CheckBox5" runat="server" Font-Bold="True" 
                            ForeColor="#506272" Text="Como Representante General" />
                    </td>
                </tr>
                <tr>
                    <td class="style67">
                        <asp:RadioButton ID="RadioButton3" runat="server" Font-Bold="True" 
                            ForeColor="#506272" Text="PRD" />
                    </td>
                    <td class="style68">
                        <asp:CheckBox ID="CheckBox3" runat="server" Font-Bold="True" 
                            ForeColor="#506272" Text="Como Voluntario en Campaña" />
                    </td>
                    <td class="style69">
                        </td>
                </tr>
                <tr>
                    <td class="style61">
                        <asp:RadioButton ID="RadioButton4" runat="server" Font-Bold="True" 
                            ForeColor="#506272" Text="Otro" />
                    </td>
                    <td class="style62">
                        <asp:CheckBox ID="CheckBox6" runat="server" Font-Bold="True" 
                            ForeColor="#506272" Text="Con Apoyo Económico" />
                    </td>
                    <td>
                        </td>
                </tr>
                <tr>
                    <td class="style61">
                        <asp:RadioButton ID="RadioButton5" runat="server" Font-Bold="True" 
                            ForeColor="#506272" Text="Indeciso" />
                    </td>
                    <td class="style62">
                        <asp:CheckBox ID="CheckBox7" runat="server" Font-Bold="True" 
                            ForeColor="#506272" Text="Prestando su Barda para Pinta" />
                    </td>
                    <td>
                        <asp:Button ID="Button2" runat="server" Text="Guardar" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    </table>
</asp:Content>

