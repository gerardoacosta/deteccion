﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mspPrincipal.master" AutoEventWireup="false" CodeFile="wfrmEstructura.aspx.vb" Inherits="Encargados_wfrmEstructura" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">


        .style2
        {
            height: 21px;
        }
    .style6
    {
        height: 15px;
    }
    .style9
    {
        height: 8px;
    }
    .style10
    {
        width: 600px;
    }
    .style12
    {
        width: 52px;
        height: 204px;
    }
        .style13
        {
            width: 110px;
            }
        .style14
        {
            height: 72px;
        }
        .style15
        {
            width: 110px;
            height: 204px;
        }
        .style16
        {
            height: 8px;
            width: 52px;
        }
        .style17
        {
            width: 691px;
        }
        .style18
        {
            width: 691px;
            height: 17px;
        }
        .style19
        {
            width: 186px;
            height: 49px;
        }
        .style20
        {
            height: 49px;
        }
        .style21
        {
            width: 272px;
        }
        .style22
        {
            width: 470px;
        }
        .style23
        {
            width: 186px;
            height: 588px;
        }
        .style26
        {
            width: 186px;
            height: 18px;
        }
        .style27
        {
            height: 18px;
        }
        .style28
        {
            width: 186px;
            height: 16px;
        }
        .style29
        {
            height: 16px;
        }
        .style30
        {
            width: 186px;
            height: 13px;
        }
        .style31
        {
            height: 13px;
        }
        .style32
        {
            width: 186px;
            height: 15px;
        }
        .style33
        {
            height: 15px;
        }
        .style34
        {
            width: 186px;
            height: 8px;
        }
        .style35
        {
            height: 8px;
        }
        .style36
        {
            height: 588px;
        }
        .style38
        {
            width: 121px;
        }
        .style39
        {
            width: 186px;
        }
        .style40
        {
            width: 82px;
            height: 17px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <table style="width:100%; height: 100%;">
    <tr>
        <td colspan="3" align="center" class="style14" 
            style="background-image: url('../Imagenes/Fondo_Subtitulo.png')">
            <asp:Label ID="Label2" runat="server" Font-Bold="True" Font-Size="Large" 
                    Text="Estructura Territorial" ForeColor="#FF9933"></asp:Label>
        </td>
    </tr>
    <tr>
        <td class="style39">
        </td>
        <td class="style13">
            <asp:Label ID="Label5" runat="server" Font-Bold="True" Font-Size="Medium" 
                Text="Responsable" ForeColor="#506272"></asp:Label>
        </td>
        <td class="style38">
            </td>
    </tr>
    <tr>
        <td class="style21" valign="top">
                            <asp:Panel ID="pnlArbol" runat="server">
                                <asp:TreeView ID="trvTerritorio" runat="server" 
    Height="370px" Width="245px" 
                                ShowLines="True">
                                    <Nodes>
                                        <asp:TreeNode Target="D" Text="Distrito 1" Value="1">
                                            <asp:TreeNode Target="SS" Text="Sec 1, 2, 3, 4, 5" Value="1, 2, 3, 4, 5">
                                                <asp:TreeNode Target="S" Text="Sec 1" Value="1">
                                                    <asp:TreeNode Target="MM" Text="Mzas 1,2,3" Value="1,2,3">
                                                        <asp:TreeNode Target="M" Text="Mza 1" Value="1"></asp:TreeNode>
                                                    </asp:TreeNode>
                                                </asp:TreeNode>
                                            </asp:TreeNode>
                                        </asp:TreeNode>
                                        <asp:TreeNode Target="D" Text="Distrito 2" Value="2"></asp:TreeNode>
                                    </Nodes>
                                    <SelectedNodeStyle BorderColor="#99CCFF" />
                                </asp:TreeView>
                            </asp:Panel>
        </td>
        <td align="left" class="style22" colspan="2" valign="top">
            <table style="height: 3955px; margin-bottom: 0px">
                <tr>
                    <td align="left" class="style26">
                        <asp:Label ID="Label6" runat="server" Font-Bold="True" Text="Nombre:" 
                            ForeColor="#506272"></asp:Label>
                    </td>
                    <td class="style27">
                        <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
&nbsp;<asp:TextBox ID="TextBox5" runat="server"></asp:TextBox>
&nbsp;<asp:TextBox ID="TextBox6" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="left" class="style18" valign="top">
                        <asp:Label ID="Label8" runat="server" Font-Bold="True" Text="Dirección:" 
                            ForeColor="#506272"></asp:Label>
                    </td>
                    <td class="style40">
                        <asp:TextBox ID="TextBox9" runat="server" Height="68px" TextMode="MultiLine" 
                            Width="392px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="left" class="style28">
                        <asp:Label ID="Label7" runat="server" Font-Bold="True" Text="Teléfono Casa:" 
                            ForeColor="#506272"></asp:Label>
                    </td>
                    <td class="style29">
                        <asp:TextBox ID="TextBox3" runat="server" Width="204px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="left" class="style26">
                        <asp:Label ID="Label10" runat="server" Font-Bold="True" 
                            Text="Teléfono Celular:" ForeColor="#506272"></asp:Label>
                    </td>
                    <td class="style27">
                        <asp:TextBox ID="TextBox4" runat="server" Width="204px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="left" class="style26">
                        <asp:Label ID="Label14" runat="server" Font-Bold="True" 
                            Text="E-mail:" ForeColor="#506272"></asp:Label>
                    </td>
                    <td class="style27">
                        <asp:TextBox ID="TextBox10" runat="server" Width="204px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="left" class="style30">
                        <asp:Label ID="Label11" runat="server" Font-Bold="True" 
                            Text="Usuario:" ForeColor="#506272"></asp:Label>
                    </td>
                    <td class="style31">
                        <asp:TextBox ID="TextBox7" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="left" class="style32">
                        <asp:Label ID="Label12" runat="server" Font-Bold="True" 
                            Text="Clave:" ForeColor="#506272"></asp:Label>
                    </td>
                    <td class="style33">
                        <asp:TextBox ID="TextBox8" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="left" class="style34">
                        <asp:Label ID="Label13" runat="server" Font-Bold="True" 
                            Text="Usuario Activo:" ForeColor="#506272"></asp:Label>
                    </td>
                    <td class="style35">
                        <asp:CheckBox ID="CheckBox1" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td align="left" class="style23">
                        </td>
                    <td class="style36">
                        </td>
                </tr>
                <tr>
                    <td align="left" class="style19">
                        </td>
                    <td class="style20">
                        <asp:Button ID="btnGuardar" runat="server" Text="Guardar" Width="77px" />
&nbsp;&nbsp;&nbsp;
                        <asp:Button ID="btnCancelar" runat="server" Text="Cancelar" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="style2" colspan="3">
&nbsp;&nbsp;&nbsp;
                </td>
    </tr>
</table>
</asp:Content>

