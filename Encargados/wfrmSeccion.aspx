﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mspPrincipal.master" AutoEventWireup="false" CodeFile="wfrmSeccion.aspx.vb" Inherits="Encargados_wfrmSeccion" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">


        .style2
        {
            height: 21px;
        }
    .style3
    {
        width: 67px;
    }
    .style5
    {
        width: 126px;
        height: 15px;
    }
    .style6
    {
        height: 15px;
    }
    .style7
    {
        width: 67px;
        height: 6px;
    }
    .style8
    {
        height: 6px;
    }
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <table style="width:100%;">
    <tr>
        <td colspan="3">
            <asp:Label ID="Label2" runat="server" Font-Bold="True" Font-Size="Large" 
                    Text="Encargados de Sección" ForeColor="#FF9933"></asp:Label>
        </td>
    </tr>
    <tr>
        <td class="style7">
            <asp:Label ID="Label3" runat="server" Text="Región:" Font-Bold="True" 
                ForeColor="#506272"></asp:Label>
        </td>
        <td class="style8">
            <asp:DropDownList ID="DropDownList1" runat="server" Width="191px">
            </asp:DropDownList>
        </td>
        <td rowspan="5">
            <table style="width:100%;">
                <tr>
                    <td align="left" class="style4">
                        <asp:Label ID="Label14" runat="server" Font-Bold="True" Text="Nombre:" 
                            ForeColor="#506272"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
&nbsp;<asp:TextBox ID="TextBox5" runat="server"></asp:TextBox>
&nbsp;<asp:TextBox ID="TextBox6" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="left" class="style5" valign="top">
                        <asp:Label ID="Label8" runat="server" Font-Bold="True" Text="Dirección:" 
                            ForeColor="#506272"></asp:Label>
                    </td>
                    <td class="style6">
                        <asp:TextBox ID="TextBox9" runat="server" Height="68px" TextMode="MultiLine" 
                            Width="392px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="left" class="style4">
                        <asp:Label ID="Label7" runat="server" Font-Bold="True" Text="Teléfono Casa:" 
                            ForeColor="#506272"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="TextBox3" runat="server" Width="204px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="left" class="style4">
                        <asp:Label ID="Label10" runat="server" Font-Bold="True" 
                            Text="Teléfono Celular:" ForeColor="#506272"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="TextBox4" runat="server" Width="204px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="left" class="style4">
                        <asp:Label ID="Label11" runat="server" Font-Bold="True" 
                            Text="Usuario:" ForeColor="#506272"></asp:Label>
                    </td>
                    <td>
&nbsp;<asp:TextBox ID="TextBox7" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="left" class="style4">
                        <asp:Label ID="Label12" runat="server" Font-Bold="True" 
                            Text="Clave:" ForeColor="#506272"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="TextBox8" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="left" class="style4">
                        <asp:Label ID="Label13" runat="server" Font-Bold="True" 
                            Text="Usuario Activo:" ForeColor="#506272"></asp:Label>
                    </td>
                    <td>
                        <asp:CheckBox ID="CheckBox1" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td align="left" class="style4">
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td align="left" class="style4">
                        &nbsp;</td>
                    <td>
                        <asp:Button ID="btnGuardar" runat="server" Text="Guardar" Width="77px" />
&nbsp;&nbsp;&nbsp;
                        <asp:Button ID="btnCancelar" runat="server" Text="Cancelar" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="style3">
            <asp:Label ID="Label4" runat="server" Text="Zona:" Font-Bold="True" 
                ForeColor="#506272"></asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="DropDownList2" runat="server" Width="191px">
            </asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td class="style3">
            <asp:Label ID="Label5" runat="server" Text="Sector:" Font-Bold="True" 
                ForeColor="#506272"></asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="DropDownList3" runat="server" Width="191px">
            </asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td class="style3">
            <asp:Label ID="Label6" runat="server" Text="Secciones:" Font-Bold="True" 
                ForeColor="#506272"></asp:Label>
        </td>
        <td rowspan="2">
            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False">
                <Columns>
                    <asp:BoundField HeaderText="Sección" />
                    <asp:BoundField HeaderText="Responsable" />
                </Columns>
            </asp:GridView>
        </td>
    </tr>
    <tr>
        <td class="style3" valign="top">
            &nbsp;</td>
    </tr>
    <tr>
        <td class="style2" colspan="3">
        </td>
    </tr>
    <tr>
        <td class="style2" colspan="3">
&nbsp;&nbsp;&nbsp;
                </td>
    </tr>
</table>
</asp:Content>

