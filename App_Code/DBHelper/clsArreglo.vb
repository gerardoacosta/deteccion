Imports System

Public Class clsArreglo

    Private intRenglon As Integer
    Private arrKeys(0) As String
    Private arrValues(0) As Object
    Public Sub Add(ByVal strClave As String, ByVal objObjeto As Object)
        Try
            If Existe(strClave) Then Throw New Exception("La clave " & strClave & " ya existe en el arreglo de parámetros")
            If intRenglon > 0 Then
                ReDim Preserve arrKeys(intRenglon)
                ReDim Preserve arrValues(intRenglon)
            End If
            arrKeys(intRenglon) = strClave
            arrValues(intRenglon) = objObjeto
            intRenglon += 1
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Sub

    Public Sub Clear()
        Array.Clear(arrKeys, 0, arrKeys.Length)
        Array.Clear(arrValues, 0, arrValues.Length)
        intRenglon = 0
    End Sub

    Public ReadOnly Property Length() As Integer
        Get
            Return intRenglon
        End Get
    End Property

    Public ReadOnly Property Key(ByVal intPos As Integer) As String
        Get
            Return arrKeys(intPos)
        End Get
    End Property

    Public ReadOnly Property Value(ByVal intPos As Integer) As Object
        Get
            Return arrValues(intPos)
        End Get
    End Property

    Public Sub New()
        intRenglon = 0
    End Sub

    Private Function Existe(ByVal strClave As String) As Boolean
        Dim intX As Integer
        For intX = 0 To arrKeys.Length - 1
            If arrKeys(intX) = strClave Then Return True
        Next
        Return False
    End Function


End Class
