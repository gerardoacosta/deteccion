Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Data.SqlDbType
Imports System

Public Class clsAdmonSQL

    Public Enum Trans
        Commit = 0
        RollBack = 1
    End Enum

    Private strConexion As String
    Private strFomatoFecha As String
    Private objConnection As SqlConnection
    Private objTransaccion As SqlTransaction
    Private intOpcion As Integer = Nothing
    Private objParametros As clsArreglo
    Private intModulo As Integer

    Public Enum Movimiento
        Inserta = 0
        Actualiza = 1
        Elimina = 2
        Ejecuta_SP = 3
        Ninguno = 4
    End Enum

    Public Enum BasedeDatos
        Cadenas = 1
    End Enum

    Public Sub Add(ByVal strClave As String, ByVal objOjeto As Object)
        Try
            objParametros.Add(strClave, objOjeto)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Sub


    Public ReadOnly Property Connection() As SqlConnection
        Get
            Return objConnection
        End Get
    End Property

    Public ReadOnly Property Transaction() As SqlTransaction
        Get
            Return objTransaccion
        End Get
    End Property

    Private Sub ConnectionString(ByVal BD As BasedeDatos)
        Select Case BD
            Case BasedeDatos.Cadenas
                strConexion = ConfigurationManager.ConnectionStrings("CONEXION").ConnectionString
        End Select

    End Sub

    Public Sub New(ByVal BD As BasedeDatos)
        objParametros = New clsArreglo
        ConnectionString(BD)
    End Sub

    Private Sub Covierte(ByRef strCadena As String, ByRef Comando As SqlCommand)
        Dim intCont, intX, intY As Integer
        Dim oprParametro As SqlParameter
        Dim strKey As String
        Dim strNuevo As String
        Dim strCadNew As String

        Try
            If strCadena = "" Then Throw New Exception("La cadena SQL es requerida")
            If strCadena.IndexOf("?") >= 0 And objParametros.Length = 0 Then Throw New Exception("Los parámetros son requeridos")

            For intX = 1 To strCadena.Length
                If strCadena.Substring(intX, 1) = "?" Then
                    intCont += 1
                End If
            Next

            If intCont <> objParametros.Length Then Throw New Exception("Los parámetros no concuerdan con los tokens")
            If Comando Is Nothing Then Throw New Exception("El OLEDBComand es requerido")

            strCadNew = ""
            intY = 0
            For intX = 1 To strCadena.Length
                If strCadena.Substring(intX, 1) = "?" Then
                    strNuevo = "@" & objParametros.Key(intY)
                    intY += 1
                    strCadNew &= strNuevo
                Else
                    strCadNew &= strCadena.Substring(intX, 1)
                End If
            Next

            For intX = 0 To objParametros.Length - 1
                strKey = objParametros.Key(intX)
                If IsToupper(strKey.Substring(3, 1)) Then
                    strKey = strKey.Substring(1, 2)
                    Select Case strKey
                        Case "ch"
                            oprParametro = New SqlParameter("@" & objParametros.Key(intX), Data.SqlDbType.Char)
                        Case Else
                            Throw New Exception("Hay un tipo de dato no existente")
                    End Select
                ElseIf IsToupper(strKey.Substring(4, 1)) Then
                    strKey = strKey.Substring(1, 3)
                    Select Case strKey
                        Case "bin"
                            oprParametro = New SqlParameter("@" & objParametros.Key(intX), System.Data.SqlDbType.Binary)
                        Case "bit"
                            oprParametro = New SqlParameter("@" & objParametros.Key(intX), System.Data.SqlDbType.Bit)
                        Case "dat"
                            oprParametro = New SqlParameter("@" & objParametros.Key(intX), System.Data.SqlDbType.DateTime)
                        Case "dec"
                            oprParametro = New SqlParameter("@" & objParametros.Key(intX), System.Data.SqlDbType.Decimal)
                        Case "flt"
                            oprParametro = New SqlParameter("@" & objParametros.Key(intX), System.Data.SqlDbType.Float)
                        Case "int"
                            oprParametro = New SqlParameter("@" & objParametros.Key(intX), System.Data.SqlDbType.Int)
                        Case "mny"
                            oprParametro = New SqlParameter("@" & objParametros.Key(intX), System.Data.SqlDbType.Money)
                        Case "nch"
                            oprParametro = New SqlParameter("@" & objParametros.Key(intX), System.Data.SqlDbType.NChar)
                        Case "num"
                            oprParametro = New SqlParameter("@" & objParametros.Key(intX), System.Data.SqlDbType.Float)
                        Case "rea"
                            oprParametro = New SqlParameter("@" & objParametros.Key(intX), System.Data.SqlDbType.Real)
                        Case "txt"
                            oprParametro = New SqlParameter("@" & objParametros.Key(intX), System.Data.SqlDbType.Text)
                        Case "tmp"
                            oprParametro = New SqlParameter("@" & objParametros.Key(intX), System.Data.SqlDbType.Timestamp)
                        Case "ide"
                            oprParametro = New SqlParameter("@" & objParametros.Key(intX), System.Data.SqlDbType.UniqueIdentifier)
                        Case "vbn"
                            oprParametro = New SqlParameter("@" & objParametros.Key(intX), System.Data.SqlDbType.VarBinary)
                        Case "vch"
                            oprParametro = New SqlParameter("@" & objParametros.Key(intX), System.Data.SqlDbType.VarChar)
                    End Select
                ElseIf IsToupper(strKey.Substring(5, 1)) Then
                    strKey = strKey.Substring(1, 4)
                    Select Case strKey
                        Case "bint"
                            oprParametro = New SqlParameter("@" & objParametros.Key(intX), System.Data.SqlDbType.BigInt)
                        Case "ntxt"
                            oprParametro = New SqlParameter("@" & objParametros.Key(intX), System.Data.SqlDbType.NText)
                        Case "nvch"
                            oprParametro = New SqlParameter("@" & objParametros.Key(intX), System.Data.SqlDbType.NVarChar)
                        Case "sdat"
                            oprParametro = New SqlParameter("@" & objParametros.Key(intX), System.Data.SqlDbType.SmallDateTime)
                        Case "sint"
                            oprParametro = New SqlParameter("@" & objParametros.Key(intX), System.Data.SqlDbType.SmallInt)
                        Case "smny"
                            oprParametro = New SqlParameter("@" & objParametros.Key(intX), System.Data.SqlDbType.SmallMoney)
                        Case "sqlv"
                            oprParametro = New SqlParameter("@" & objParametros.Key(intX), System.Data.SqlDbType.Variant)
                        Case "tint"
                            oprParametro = New SqlParameter("@" & objParametros.Key(intX), System.Data.SqlDbType.TinyInt)
                    End Select
                Else
                    Throw New Exception("Hay un tipo de dato no existente Metodo:Covierte Clase: Helper.clsAdmonSql")
                End If
                oprParametro.Value = objParametros.Value(intX)
                Comando.Parameters.Add(oprParametro)
            Next

            strCadena = strCadNew
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Sub

    Private Function IsToupper(ByVal chCar As Char) As Boolean
        If Asc(chCar) >= 65 And Asc(chCar) <= 90 Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function Query(ByVal strQuery As String, _
                          Optional ByRef dsDatos As Data.DataSet = Nothing, _
                          Optional ByVal strNombre As String = "", _
                          Optional ByVal blnParametros As Boolean = False) As Data.DataSet
        Dim objConn As SqlConnection
        Dim odaAdaptador As SqlDataAdapter
        Dim dsDataset As New Data.DataSet
        Try
            Dim cmd As New SqlCommand
            If Not objConnection Is Nothing Then
                cmd.Connection = objConnection
            Else
                objConn = New SqlConnection(strConexion)
                objConn.Open()
                cmd.Connection = objConn
            End If

            If Not objTransaccion Is Nothing Then
                cmd.Transaction = objTransaccion
            End If

            cmd.CommandType = System.Data.CommandType.Text

            If blnParametros Then Covierte(strQuery, cmd)
            cmd.CommandText = strQuery

            odaAdaptador = New SqlDataAdapter(cmd)

            If Not dsDatos Is Nothing Then
                dsDataset = dsDatos
            End If

            If strNombre <> "" Then
                odaAdaptador.Fill(dsDataset, strNombre)
            Else
                odaAdaptador.Fill(dsDataset)
            End If

            Return dsDataset

        Catch ex As SqlException
            Throw New Exception(ex.Message)
        Finally
            objParametros.Clear()
            If objConnection Is Nothing Then
                objConn.Close()
            End If
        End Try
    End Function

    Public Sub ExecuteQuery(ByVal strQuery As String, _
                            Optional ByVal blnCerrar As Boolean = False, _
                            Optional ByVal blnParametros As Boolean = False, _
                            Optional ByVal Movimiento As Movimiento = Movimiento.Ninguno, _
                            Optional ByVal strDescripcion As String = "", _
                            Optional ByVal strusuario As String = "")
        Dim objConn As SqlConnection

        Try
            Dim cmd As New SqlCommand
            If Not objConnection Is Nothing Then
                cmd.Connection = objConnection
            Else
                If blnCerrar Then
                    If objConnection Is Nothing Then
                        objConnection = New SqlConnection(strConexion)
                        objConnection.Open()
                        cmd.Connection = objConnection
                    Else
                        cmd.Connection = objConnection
                    End If
                Else
                    objConn = New SqlConnection(strConexion)
                    objConn.Open()
                    cmd.Connection = objConn
                End If
            End If

            If Not objTransaccion Is Nothing Then
                cmd.Transaction = objTransaccion
            End If

            cmd.CommandType = System.Data.CommandType.Text

            If blnParametros Then Covierte(strQuery, cmd)
            cmd.CommandText = strQuery

            cmd.ExecuteNonQuery()
            ''Me.Bitacora(Movimiento, strDescripcion, strusuario)

        Catch ex As SqlException
            Throw New Exception(ex.Message)
        Finally
            objParametros.Clear()
            If objConnection Is Nothing And blnCerrar = False Then
                objConn.Close()
            End If
        End Try
    End Sub


    Public Sub Bitacora(ByVal Mov As Movimiento, _
                        ByVal strDescripcion As String, _
                        ByVal strUsuario As String)
        Dim objConn As SqlConnection
        Dim strBitacora As String
        Try
            Dim cmd As New SqlCommand


            objConn = New SqlConnection(strConexion)
            objConn.Open()

            strBitacora = "INSERT INTO tbl_Bitacora (vchMovimiento, sdatFecha, vchDescripcion, vchUsuario) VALUES("
            strBitacora += "'" & Mov.ToString & "', GETDATE(), '" & strDescripcion & "', '" & strUsuario & "')"
            cmd.Connection = objConn
            cmd.CommandType = System.Data.CommandType.Text
            cmd.CommandText = strBitacora
            cmd.ExecuteNonQuery()


        Catch ex As SqlException
            Throw New Exception(ex.Message)
        Finally
            If Not objConn Is Nothing Then
                objConn.Close()
            End If
        End Try
    End Sub

    Public Function ExecuteScalar(ByVal strQuery As String, _
                                  Optional ByVal blnCerrar As Boolean = False, _
                                  Optional ByVal blnParametros As Boolean = False) As Object
        Dim objConn As SqlConnection
        Dim objRet As Object
        Try
            Dim cmd As New SqlCommand
            If Not objConnection Is Nothing Then
                cmd.Connection = objConnection
            Else
                If blnCerrar Then
                    If objConnection Is Nothing Then
                        objConnection = New SqlConnection(strConexion)
                        objConnection.Open()
                        cmd.Connection = objConnection
                    Else
                        cmd.Connection = objConnection
                    End If
                Else
                    objConn = New SqlConnection(strConexion)
                    objConn.Open()
                    cmd.Connection = objConn
                End If
            End If

            If Not objTransaccion Is Nothing Then
                cmd.Transaction = objTransaccion
            End If

            cmd.CommandType = System.Data.CommandType.Text

            If blnParametros Then Covierte(strQuery, cmd)
            cmd.CommandText = strQuery

            objRet = cmd.ExecuteScalar

            Return objRet

        Catch ex As SqlException
            Throw New Exception(ex.Message)
        Finally
            objParametros.Clear()
            If objConnection Is Nothing And blnCerrar = False Then
                If Not objConnection Is Nothing Then objConn.Close()
            End If
        End Try
    End Function

    Public Function ExecuteReader(ByVal strQuery As String, _
                                  Optional ByVal blnCerrar As Boolean = False, _
                                  Optional ByVal blnParametros As Boolean = False) As SqlDataReader
        Dim objRet As SqlDataReader
        Dim objConn As SqlConnection
        Try
            Dim cmd As New SqlCommand
            If Not objConnection Is Nothing Then
                cmd.Connection = objConnection
            Else
                If blnCerrar Then
                    If objConnection Is Nothing Then
                        objConnection = New SqlConnection(strConexion)
                        objConnection.Open()
                        cmd.Connection = objConnection
                    Else
                        cmd.Connection = objConnection
                    End If
                Else
                    objConn = New SqlConnection(strConexion)
                    objConn.Open()
                    cmd.Connection = objConn
                End If
            End If

            If Not objTransaccion Is Nothing Then
                cmd.Transaction = objTransaccion
            End If

            cmd.CommandType = System.Data.CommandType.Text

            If blnParametros Then Covierte(strQuery, cmd)
            cmd.CommandText = strQuery

            objRet = cmd.ExecuteReader

            Return objRet
        Catch ex As SqlException
            Throw New Exception(ex.Message)
        Finally
            objParametros.Clear()
            If objConnection Is Nothing And blnCerrar = False Then
                If Not objConnection Is Nothing Then objConn.Close()
            End If
        End Try
    End Function

    Public Sub Close()
        objParametros.Clear()
        If Not objConnection Is Nothing Then
            If objConnection.State = System.Data.ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
            objConnection = Nothing
        End If
    End Sub

    Public Sub Open()
        Try
            If objConnection Is Nothing Then
                objConnection = New SqlConnection(strConexion)
                objConnection.Open()
            Else
                If objConnection.State = System.Data.ConnectionState.Closed Then objConnection.Open()
            End If
        Catch ex As SqlException
            Throw New Exception(ex.Message)
        End Try
    End Sub

    Public Sub Transaccion()
        Try
            If Not objConnection Is Nothing Then
                If objConnection.State = System.Data.ConnectionState.Closed Then objConnection.Open()
                objTransaccion = objConnection.BeginTransaction
            Else
                Throw New Exception("La conexion no existe")
            End If
        Catch ex As SqlException
            Throw New Exception(ex.Message)
        End Try
    End Sub

    Public Sub TransacOperation(ByVal op As Trans)
        Select Case op
            Case Trans.Commit
                If Not objTransaccion Is Nothing Then objTransaccion.Commit()
            Case Trans.RollBack
                If Not objTransaccion Is Nothing Then objTransaccion.Rollback()
        End Select
        objTransaccion = Nothing
    End Sub
End Class
