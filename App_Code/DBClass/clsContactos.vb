Imports Microsoft.VisualBasic
Imports System

Public Class clsContactos
    Inherits clsAdmonSQL
    Dim strQuery As String

    Public Overloads Function Dame_Datos() As Data.DataSet
        Try
            strQuery = "SELECT *"
            strQuery += " FROM tbl_Contactos"
            Return Me.Query(strQuery, , , False)
        Catch ex As Exception

        End Try
    End Function

    Public Overloads Function Dame_Datos(ByVal intContactoID As String) As Data.DataSet
        Try
            strQuery = "SELECT *"
            strQuery += " FROM tbl_Contactos"
            strQuery += " WHERE intContactoID = " & intContactoID
            Return Me.Query(strQuery, , , False)
        Catch ex As Exception

        End Try
    End Function

    ''Public Overloads Function Dame_Contactos(ByVal intContactoID As Integer) As Data.DataSet
    ''    Try
    ''        strQuery = "SELECT strNombre + ' ' + strAPaterno + ' ' + strAMaterno as Nombre, strTelefono, strCelular, strEmail, C.intContactoID"
    ''        strQuery += " FROM tbl_Compromisos C, tbl_Contactos CC"
    ''        strQuery += " WHERE C.intContactoID IN (SELECT intContactoID FROM tbl_Cadenas WHERE intPadreID = " & intContactoID & ")"
    ''        strQuery += " AND CC.intContactoID = C.intContactoID"
    ''        Return Me.Query(strQuery, , , False)
    ''    Catch ex As Exception
    ''        Throw New Exception(ex.Message)
    ''    End Try
    ''End Function

    Public Overloads Function Dame_ContactosCadena(ByVal strCadena As Integer) As Data.DataSet
        Try
            strQuery = "SELECT strNombre + ' ' + strAPaterno + ' ' + strAMaterno as Nombre, strTelefono, strTelefono2, strCelular, strEmail, intContactoID"
            strQuery += " FROM tbl_Contactos CC"
            strQuery += " WHERE intContactoID IN (SELECT intContactoID FROM tbl_Cadenas WHERE intCadena = " & strCadena & ")"
            strQuery += " ORDER BY Nombre"
            Return Me.Query(strQuery, , , False)
        Catch ex As Exception

        End Try
    End Function

    Public Overloads Function Dame_Contactos(ByVal intContactoID As Integer) As Data.DataSet
        Try
            strQuery = "SELECT strNombre + ' ' + strAPaterno + ' ' + strAMaterno as Nombre, strTelefono, strTelefono2, strCelular, strEmail, intContactoID"
            strQuery += " FROM tbl_Contactos CC"
            strQuery += " WHERE intContactoID IN (SELECT intContactoID FROM tbl_Cadenas WHERE intPadreID = " & intContactoID & ")"
            strQuery += " ORDER BY Nombre"
            Return Me.Query(strQuery, , , False)
        Catch ex As Exception

        End Try
    End Function

    Public Overloads Function Dame_DatosContactos(ByVal intContactoID As Integer) As Data.DataSet
        Try
            strQuery = "SELECT CC.*, C.strColonia"
            strQuery += " FROM tbl_Contactos CC, tbl_Cat_Colonias C"
            strQuery += " WHERE intContactoID IN (SELECT intContactoID FROM tbl_Cadenas WHERE intPadreID = " & intContactoID & ")"
            strQuery += " AND C.intColonia = CC.intColonia"
            strQuery += " ORDER BY strAPaterno, strAMaterno, strNombre"
            Return Me.Query(strQuery, , , False)
        Catch ex As Exception

        End Try
    End Function

    Public Overloads Function Dame_Emails(ByVal intContactoID As Integer) As Data.DataSet
        Try
            strQuery = "SELECT strNombre + ' ' + strAPaterno + ' ' + strAMaterno as Nombre, strTelefono, strCelular, strEmail, C.intContactoID"
            strQuery += " FROM tbl_Compromisos C, tbl_Contactos CC"
            strQuery += " WHERE C.intContactoID IN (SELECT intContactoID FROM tbl_Cadenas WHERE intPadreID = " & intContactoID & ")"
            strQuery += " AND CC.intContactoID = C.intContactoID"
            strQuery += " AND CC.strEmail <> ''"
            Return Me.Query(strQuery, , , False)
        Catch ex As Exception

        End Try
    End Function

    Public Sub New(ByVal bd As BasedeDatos)
        MyBase.New(bd)
    End Sub
End Class
