Imports Microsoft.VisualBasic
Imports System

Public Class clsDeteccion
    Inherits clsAdmonSQL
    Dim strQuery As String

    Public Overloads Function Dame_Datos(ByVal intContactoID As String) As Data.DataSet
        Try
            strQuery = "SELECT *"
            strQuery += " FROM tbl_Deteccion"
            strQuery += " where intContactoID = " & intContactoID
            Return Me.Query(strQuery, , , False)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Overloads Function Inserta_Datos(ByVal intContactoID As String, ByVal intBarrido As String, ByVal intDeteccion As String, ByVal strUsuario As String) As Boolean
        Try
            strQuery = "INSERT INTO tbl_Deteccion (intContactoID, intDeteccion, intBarrido, vchUsrDetecto)"
            strQuery += " FROM tbl_Deteccion"
            strQuery += " where intContactoID = " & intContactoID
            Me.ExecuteQuery(strQuery, False, False, Movimiento.Inserta, , strUsuario)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Sub New(ByVal bd As BasedeDatos)
        MyBase.New(bd)
    End Sub
End Class
