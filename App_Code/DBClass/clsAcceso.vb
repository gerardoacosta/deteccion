Imports Microsoft.VisualBasic
Imports System

Public Class clsAcceso
    Inherits clsAdmonSQL
    Dim strQuery As String

    Public Overloads Function Acceso(ByVal strUsuario As String) As Data.DataSet
        Try
            strQuery = "SELECT *"
            strQuery += " FROM tbl_Usuarios"
            strQuery += " WHERE strUsuario = '" & strUsuario & "'"
            Return Me.Query(strQuery, , , False)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Overloads Function Usuario(ByVal intContactoID As Integer) As Data.DataSet
        Try
            strQuery = "SELECT *"
            strQuery += " FROM tbl_Usuarios"
            strQuery += " WHERE intContactoID = " & intContactoID
            Return Me.Query(strQuery, , , False)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Function JCM() As Boolean
        Dim i As Integer
        For i = 1 To 4000
            strQuery = "update dbo.tbl_Compromisos set intContactos = (select count(*) from dbo.tbl_Cadenas where intPadreID = " & i & ")"
            strQuery += "where intContactoID =" & i
            Me.ExecuteQuery(strQuery, False, False, Movimiento.Ninguno, "", "")
        Next
    End Function

    Public Sub New(ByVal bd As BasedeDatos)
        MyBase.New(bd)
    End Sub
End Class
