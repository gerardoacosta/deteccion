Imports Microsoft.VisualBasic
Imports System

Public Class clsCompromisos
    Inherits clsAdmonSQL
    Dim strQuery As String

    Public Overloads Function RegistroInicio(ByVal intContactoID As String) As Boolean
        Try
            strQuery = "SELECT count(*)"
            strQuery += " FROM tbl_Compromisos"
            strQuery += " WHERE intContactoID = " & intContactoID
            Return Me.ExecuteScalar(strQuery, False, False)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Overloads Function Insertar(ByVal intContactoID As String, ByVal intCompromiso As String, _
                                        ByVal sdtFecha As String, ByVal bolC1 As String, _
                                        ByVal bolC2 As String, ByVal bolC3 As String, _
                                        ByVal bolC4 As String, ByVal bolC5 As String, _
                                        ByVal bolC6 As String, ByVal bolC7 As String, _
                                        ByVal bolC8 As String, ByVal bolC9 As String, _
                                        ByVal bolC10 As String, ByVal intAuto As String, _
                                        ByVal mnyAportacion As String, ByVal intContactos As String) As Boolean
        Try
            strQuery = "INSERT INTO tbl_Compromisos (intContactoID, intCompromiso, sdtFecha, bolC1"
            strQuery += " , bolC2, bolC3, bolC4, bolC5, bolC6, bolC7, bolC8, bolC9, bolC10, intAuto"
            strQuery += " , mnyAportacion, intContactos)"
            strQuery += " Values(" & intContactoID & "," & intCompromiso & ",'" & sdtFecha & "'"
            strQuery += " ," & bolC1 & ", " & bolC2 & ", " & bolC3 & ", " & bolC4 & ", " & bolC5 & ", " & bolC6
            strQuery += " ," & bolC7 & ", " & bolC8 & ", " & bolC9 & ", " & bolC10 & ", " & intAuto & ", " & mnyAportacion
            strQuery += " ," & intContactos & ")"
            Me.ExecuteQuery(strQuery, False, False, Movimiento.Inserta, "Compromisos de " & intCompromiso & " Fecha:" & sdtFecha, intContactoID)
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message)
            Return False
        End Try
    End Function

    Public Overloads Function Dame_Compromiso(ByVal intContactoID As String) As Data.DataSet
        Try
            strQuery = "SELECT intCompromiso, sdtFecha, intContactos, (intCompromiso-intContactos) as Faltan, (intContactos/intCompromiso)*100 AS Avance"
            strQuery += " FROM tbl_Compromisos"
            strQuery += " WHERE intContactoID = " & intContactoID
            Return Me.Query(strQuery, , , False)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Overloads Function Contabiliza_Contacto(ByVal strContacto As String) As Boolean
        Try
            strQuery = "UPDATE  tbl_Compromisos"
            strQuery += " SET intContactos = intContactos + 1"
            strQuery += " WHERE intContactoID = " & strContacto
            Me.ExecuteQuery(strQuery, False, False, Movimiento.Actualiza, "Agregar Contacto", strContacto)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Overloads Function Dame_Mis5Mejores(ByVal intContactoID As String) As Data.DataSet
        Try
            strQuery = "SELECT TOP 5 strNombre + ' ' + strAPaterno + ' ' + strAMaterno as Nombre,intCompromiso, CONVERT(CHAR(11),sdtFecha) AS sdtFecha, intContactos, (intCompromiso-intContactos) as Faltan, (intContactos/intCompromiso)*100 AS Avance"
            strQuery += " FROM tbl_Compromisos C, tbl_Contactos CC"
            strQuery += " WHERE C.intContactoID IN (SELECT intContactoID FROM tbl_Cadenas WHERE intPadreID = " & intContactoID & ")"
            strQuery += " AND CC.intContactoID = C.intContactoID"
            Return Me.Query(strQuery, , , False)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Overloads Function Dame_Los5Mejores(ByVal intContactoID As String) As Data.DataSet
        Try
            strQuery = "SELECT TOP 5 strNombre + ' ' + strAPaterno + ' ' + strAMaterno as Nombre,intCompromiso, CONVERT(CHAR(11),sdtFecha) AS sdtFecha, intContactos, (intCompromiso-intContactos) as Faltan, (intContactos/intCompromiso)*100 AS Avance"
            strQuery += " FROM tbl_Compromisos C, tbl_Contactos CC"
            strQuery += " WHERE C.intContactoID IN (SELECT intContactoID FROM tbl_Cadenas WHERE intPadreID = " & intContactoID & ")"
            strQuery += " AND CC.intContactoID = C.intContactoID"
            Return Me.Query(strQuery, , , False)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Sub New(ByVal bd As BasedeDatos)
        MyBase.New(bd)
    End Sub
End Class
