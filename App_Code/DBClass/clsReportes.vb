Imports Microsoft.VisualBasic
Imports System

Public Class clsReportes
    Inherits clsAdmonSQL
    Dim strQuery As String

    Public Overloads Function Inserta_Contactos(ByVal intUsuario As String, ByVal intContactoID As String) As Boolean
        Try
            strQuery = "INSERT INTO tbl_Listado"
            strQuery += " SELECT " & intUsuario & ", strIFE, strNombre, strAPaterno, strAMaterno, sdtFechaNac, strCalle,"
            strQuery += " strNoExt, strNoInt, strColonia, intCP, strTelefono, strTelefono2, strCelular, strEmail"
            strQuery += " FROM tbl_Contactos CC, tbl_Cat_Colonias C"
            strQuery += " WHERE intContactoID = " & intContactoID
            strQuery += " AND C.intColonia = CC.intColonia"
            Me.ExecuteQuery(strQuery, False, False, Movimiento.Inserta, "", "")

        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Overloads Function Dame_Listado() As Data.DataSet
        Try
            strQuery = "SELECT * FROM tbl_Listado"
            Return Me.Query(strQuery, , , False)

        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Overloads Function Dame_Listado(ByVal intContacto As String) As Data.DataSet
        Try
            strQuery = "SELECT * FROM tbl_Listado WHERE intContactoID = " & intContacto
            Return Me.Query(strQuery, , , False)

        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Overloads Function Limpia_Listado(ByVal intContacto As String) As Boolean
        Try
            strQuery = "DELETE FROM tbl_Listado WHERE intContactoID = " & intContacto
            Me.ExecuteQuery(strQuery, False, False, Movimiento.Elimina, "", "")

        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Overloads Function Contactos_Colonias() As Data.DataSet
        Try
            strQuery = "SELECT COUNT(intContactoID) as intContactos, strColonia FROM tbl_Contactos c, tbl_Cat_Colonias cc"
            strQuery += " WHERE cc.intColonia = c.intColonia"
            strQuery += " GROUP BY strColonia"
            strQuery += " ORDER BY strColonia"
            Return Me.Query(strQuery, , , False)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Overloads Function Total_Contactos() As Integer
        Try
            strQuery = "SELECT COUNT(intContactoID)"
            strQuery += " FROM tbl_Contactos"
            Return Me.ExecuteScalar(strQuery, False, False)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Function Apoyos(ByVal intColonia As String, ByVal intApoyo As String) As Data.DataSet
        Try
            Dim strApoyo As String
            Select Case intApoyo
                Case 1
                    strApoyo = "Repartiendo Volantes"
                Case 2
                    strApoyo = "Jornadas de Apoyo"
                Case 3
                    strApoyo = "Prestar mi barda para pinta"
                Case 4
                    strApoyo = "Asistente para Eventos"
                Case 5
                    strApoyo = "Telefonista / Call Center"
                Case 6
                    strApoyo = "Prestar mi auto"
                Case 7
                    strApoyo = "Aportaci�n econ�mica"
                Case 8
                    strApoyo = "Representante de Casilla"
                Case 9
                    strApoyo = "Apoyo en Campa�a"
                Case 10
                    strApoyo = "Encuestador"
            End Select
            strQuery = "Select C.*, COL.strColonia, '" & strApoyo & "' as strApoyo"
            strQuery += " FROM tbl_Contactos C, tbl_Cat_Colonias COL"
            strQuery += " WHERE COL.intColonia = " & intColonia
            strQuery += " AND COL.intColonia = C.intColonia"
            strQuery += " AND C.intContactoID IN (Select intContactoID from tbl_Compromisos WHERE bolC" & intApoyo & " = 1)"
            Return Me.Query(strQuery, , , False)
        Catch ex As Exception
        End Try
    End Function

    Public Sub New(ByVal bd As BasedeDatos)
        MyBase.New(bd)
    End Sub
End Class
