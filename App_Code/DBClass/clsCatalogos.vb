Imports Microsoft.VisualBasic
Imports System

Public Class clsCatalogos
    Inherits clsAdmonSQL
    Dim strQuery As String

    Public Overloads Function DameColonias() As Data.DataSet
        Try
            strQuery = "SELECT *"
            strQuery += " FROM tbl_Cat_Colonias order by strColonia"
            Return Me.Query(strQuery, , , False)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Overloads Function DameAutos() As Data.DataSet
        Try
            strQuery = "SELECT *"
            strQuery += " FROM tbl_Cat_Autos order by strAuto"
            Return Me.Query(strQuery, , , False)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Overloads Function DameCadenas() As Data.DataSet
        Try
            strQuery = "SELECT *"
            strQuery += " FROM tbl_Cat_Cadenas order by strCadena"
            Return Me.Query(strQuery, , , False)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Overloads Function DameEmails() As Data.DataSet
        Try
            strQuery = "SELECT *"
            strQuery += " FROM tbl_Cat_Email"
            Return Me.Query(strQuery, , , False)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Overloads Function DameEmail(ByVal intEmail As String) As Data.DataSet
        Try
            strQuery = "SELECT *"
            strQuery += " FROM tbl_Cat_Email"
            strQuery += " WHERE intEmail = " & intEmail
            Return Me.Query(strQuery, , , False)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Sub New(ByVal bd As BasedeDatos)
        MyBase.New(bd)
    End Sub
End Class
