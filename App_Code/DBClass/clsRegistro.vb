Imports Microsoft.VisualBasic
Imports System

Public Class clsRegistro
    Inherits clsAdmonSQL
    Dim strQuery As String

    Public Function Inserta_Contactos(ByVal strNombre As String, ByVal strAPaterno As String, ByVal strAMaterno As String, _
                                      ByVal intColonia As String, ByVal strEmail As String, _
                                      ByVal strUsuario As String, ByVal strClave As String, _
                                      ByVal strPadre As String, ByVal strCadena As String) As Boolean
        Try
            strQuery = "INSERT INTO tbl_Contactos (strNombre, strAPaterno, strAMaterno, intColonia, strEmail)"
            strQuery += " VALUES("
            strQuery += "'" & strNombre
            strQuery += "','" & strAPaterno
            strQuery += "','" & strAMaterno
            strQuery += "'," & intColonia
            strQuery += ",'" & strEmail & "')"
            Me.ExecuteQuery(strQuery, False, False, Movimiento.Inserta, strNombre & " " & strAPaterno & " " & strAMaterno, strClave)

            Dim intContacto As Integer
            strQuery = "Select intContactoID FROM tbl_Contactos "
            strQuery += " WHERE strNombre = '" & strNombre & "'"
            strQuery += " AND strAPaterno = '" & strAPaterno & "'"
            strQuery += " AND strAMaterno = '" & strAMaterno & "'"
            intContacto = Me.ExecuteScalar(strQuery, False, False)

            strQuery = "INSERT INTO tbl_Usuarios (strUsuario, strClave, intContactoID, intCadena)"
            strQuery += " VALUES('" & strUsuario
            strQuery += "','" & strClave
            strQuery += "'," & intContacto
            strQuery += "," & strCadena & ")"
            Me.ExecuteQuery(strQuery, False, False, Movimiento.Inserta, strNombre & " " & strAPaterno & " " & strAMaterno, strClave)

            strQuery = "INSERT INTO tbl_Cadenas (intContactoID, intPadreID, intCadena)"
            strQuery += " VALUES('" & intContacto & "','" & strPadre & "'," & strCadena & ")"
            Me.ExecuteQuery(strQuery, False, False, Movimiento.Inserta, strNombre & " " & strAPaterno & " " & strAMaterno, strClave)

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message)
            Return False
        End Try
    End Function

    Public Function Actualiza_Contactos(ByVal strNombre As String, ByVal strAPaterno As String, ByVal strAMaterno As String, _
                                        ByVal strFechaNac As String, ByVal strCalle As String, ByVal strNoExt As String, _
                                        ByVal strNoInt As String, ByVal intColonia As String, ByVal strTelefono As String, _
                                        ByVal strCelular As String, ByVal strEmail As String, ByVal strUsuario As String, _
                                        ByVal strClave As String, ByVal strContacto As String, ByVal strIFE As String) As Boolean
        Try
            strQuery = "UPDATE tbl_Contactos SET strNombre = '" & strNombre
            strQuery += "', strAPaterno = '" & strAPaterno
            strQuery += "', strAMaterno = '" & strAMaterno
            If IsDate(strFechaNac) Then
                strQuery += "', sdtFechaNac = '" & strFechaNac
            End If
            strQuery += "', strCalle = '" & strCalle
            strQuery += "', strNoExt = '" & strNoExt
            strQuery += "', strNoInt = '" & strNoInt
            strQuery += "', intColonia = " & intColonia
            strQuery += ", strTelefono = '" & strTelefono
            strQuery += "', strCelular = '" & strCelular
            strQuery += "', strEmail = '" & strEmail
            strQuery += "', strIFE = '" & strIFE & "'"
            strQuery += " WHERE intContactoID = " & strContacto
            Me.ExecuteQuery(strQuery, False, False, Movimiento.Actualiza, strNombre & " " & strAPaterno & " " & strAMaterno, strContacto)

            strQuery = "UPDATE tbl_Usuarios SET strUsuario = '" & strUsuario
            strQuery += "', strClave = '" & strClave & "'"
            strQuery += " WHERE intContactoID = " & strContacto
            Me.ExecuteQuery(strQuery, False, False, Movimiento.Actualiza, "Usuario:" & strUsuario & ", Clave: " & strClave, strContacto)

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message)
            Return False
        End Try
    End Function

    Public Overloads Function Dame_Datos(ByVal strContacto As String) As Data.DataSet
        Try
            strQuery = "SELECT *"
            strQuery += " FROM tbl_Contactos"
            strQuery += " WHERE intContactoID = " & strContacto
            Return Me.Query(strQuery, , , False)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Overloads Function Dame_Datos(ByVal strNombre As String, ByVal strAPaterno As String, _
                                         ByVal strAMaterno As String) As Data.DataSet
        Try
            strQuery = "SELECT *"
            strQuery += " FROM tbl_Contactos"
            strQuery += " WHERE strNombre = '" & strNombre & "'"
            strQuery += " AND strAPaterno = '" & strAPaterno & "'"
            strQuery += " AND strAMaterno = '" & strAMaterno & "'"
            Return Me.Query(strQuery, , , False)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Function Valida_Usuarios(ByVal strUsuario As String) As Boolean
        Try
            strQuery = "SELECT count(*)"
            strQuery += " FROM tbl_Usuarios"
            strQuery += " WHERE strUsuario = '" & strUsuario & "'"
            Return Me.ExecuteScalar(strQuery, False, False)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Sub New(ByVal bd As BasedeDatos)
        MyBase.New(bd)
    End Sub
End Class
