﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="wfrmLogin.aspx.vb" Inherits="Seguridad_wfrmLogin" %>
<%@ Register TagPrefix="CC1" Namespace="MsgBox" Assembly="MsgBox"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .style1
        {
        }
        .style2
        {
            height: 151px;
        }
        .style4
        {
            width: 91px;
        }
        .style5
        {
            width: 344px;
            height: 325px;
        }
        .style6
        {
            height: 325px;
        }
        .style7
        {
            width: 290px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div style="width: 815px; height: 606px;">
    
        <table style="width: 98%; background-image: url('../Imagenes/Login_01.jpg'); height: 600px;">
            <tr>
                <td class="style2" align="center" colspan="2">
                    <asp:Label ID="Label4" runat="server" Font-Bold="True" Font-Size="XX-Large" 
                        ForeColor="White" Text="SISTEMA DE POSESIONAMIENTO ELECTORAL"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style5">
                    </td>
                <td class="style6">
                    <table style="width:100%;">
                        <tr>
                            <td class="style4">
                                <asp:Label ID="Label3" runat="server" Font-Bold="True" Text="Estado:" 
                                    ForeColor="#506272"></asp:Label>
                            </td>
                            <td class="style7">
                                <asp:DropDownList ID="ddlEstado" runat="server" Height="22px" Width="171px">
                                    <asp:ListItem Value="01">Aguascalientes</asp:ListItem>
                                    <asp:ListItem Value="02">Baja Californía</asp:ListItem>
                                    <asp:ListItem Value="03">Baja Californía Sur</asp:ListItem>
                                    <asp:ListItem Value="16">Michoacán</asp:ListItem>
                                    <asp:ListItem Value="0">---- SELECCIONAR ----</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="style4">
                                <asp:Label ID="Label1" runat="server" Font-Bold="True" Text="Usuario:" 
                                    ForeColor="#506272"></asp:Label>
                            </td>
                            <td class="style7">
                                <asp:TextBox ID="txtUser" runat="server" MaxLength="10"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="style4">
                                <asp:Label ID="Label2" runat="server" Font-Bold="True" Text="Clave:" 
                                    ForeColor="#506272"></asp:Label>
                            </td>
                            <td class="style7">
                                <asp:TextBox ID="txtClave" runat="server" MaxLength="10" TextMode="Password"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="style4">
                                &nbsp;</td>
                            <td class="style7">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td class="style4">
                                <CC1:MsgBox ID="msgAlert" runat="Server" Style="z-index: 101; left: 16px; position: absolute;
                    top: 8px" />
                            </td>
                            <td class="style7">
                                <asp:Button ID="btnAceptar" runat="server" Text="Aceptar" Width="77px" />
&nbsp;&nbsp;&nbsp;
                                <asp:Button ID="btnCancelar" runat="server" Text="Cancelar" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="style1" colspan="2" valign="middle">
                    <asp:Label ID="Label5" runat="server" Font-Bold="True" Font-Size="X-Large" 
                        ForeColor="White" Text="DETECCIÓN DE SIMPATIA"></asp:Label>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
