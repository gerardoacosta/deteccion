﻿
Partial Class Seguridad_wfrmLogin
    Inherits System.Web.UI.Page

    Protected Sub btnAceptar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        If Me.ddlEstado.SelectedValue = 0 Then
            Me.msgAlert.ShowMessage("Debe indica el estado para ingresar.")
            Exit Sub
        End If

        If Me.txtUser.Text.Trim = "" Or Me.txtUser.Text.Trim.ToUpper <> "SIPE" Then
            Me.msgAlert.ShowMessage("Debe indica un Usuario valido para ingresar.")
            Exit Sub
        End If

        If Me.txtClave.Text.Trim = "" Or Me.txtClave.Text.Trim.ToUpper <> "PERRO" Then
            Me.msgAlert.ShowMessage("Debe indica la clave para ingresar.")
            Exit Sub
        End If


        Session("Estado") = Me.ddlEstado.SelectedValue
        Session("NomEstado") = Me.ddlEstado.SelectedItem.Text
        Session("NomUser") = "DEMOSTRACION SIPE"
        Response.Redirect("../wfrmMenu.aspx")

    End Sub
End Class
